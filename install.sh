#########################
### Library Generator ###
#########################

# Library name
name=alloy
# Library header path
header=src/alloy.h
# Library source path
source=src/alloy.cpp
# Library build directory path
dist=./build

### --- Code Logic --- ###

# Linux case
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    echo "--- Distributing for linux ---"
    printf '  |░░░░░░░░░░░░░░░░░░░░░░░░|\r  |'
    clang++ -shared -fPIC -o ${dist}/lib${name}.so ${source} -ferror-limit=2 -Wfatal-errors -std=c++2a -ldl -Ofast
    printf '██████'
    clang++ -c ${source} -fPIC -o ${dist}/tmp.o -ferror-limit=2 -Wfatal-errors -std=c++2a -Ofast
    printf '██████'
    llvm-ar rcs ${dist}/lib${name}.a ${dist}/tmp.o
    printf '██████| Done 🎉\n'
    python3 headerize.py ${header} ${dist}/${name}.hpp
    rm ${dist}/tmp.o
# Windows case (run with Git Bash)
elif [[ "$OSTYPE" == "msys" ]]; then
    echo "--- Distributing for windows ---"
    printf '  |░░░░░░░░░░░░░░░░░░░░░░░░|\r  |'
    clang++ -shared -o ${dist}/lib${name}.dll ${source} -ferror-limit=2 -Wfatal-errors -std=c++2a -lShell32 -lUser32 -Ofast -Wno-deprecated-declarations
    printf '██████'
    clang++ -c ${source} -o ${dist}/tmp.o -ferror-limit=2 -Wfatal-errors -std=c++2a -Ofast -Wno-deprecated-declarations
    printf '██████'
    llvm-ar rcs ${dist}/lib${name}.lib ${dist}/tmp.o
    printf '██████| Done 🎉\n'
    python headerize.py ${header} ${dist}/${name}.hpp
    rm ${dist}/tmp.o
fi
