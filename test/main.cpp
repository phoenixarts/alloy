#include "../src/alloy.cpp"
using namespace Alloy;

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

int main(int argc, char** argv) {
    Process::init(argc, argv);

    Text exe = Path::execPath();
    Text path = Path::join({exe, "../", "libshared.so"});

    char rawpath[512];
    std::strcpy(rawpath, path.cstr());
    void* handle = dlopen (rawpath, RTLD_LAZY);

    if (!handle) {
        fprintf (stderr, "%s\n", dlerror());
        exit(1);
    }

    dlerror();

    void* test = dlsym(handle, "hello");

    // typedef Tree (*fptr)(Tree);

    Tree (*my_ptr)(Tree) {};
    reinterpret_cast<void*&>(my_ptr) = test; 


    char* error;
    if ((error = dlerror()) != NULL)  {
        fprintf (stderr, "%s\n", error);
        exit(1);
    }
    (*my_ptr)("data = 'Pawel'").log();

    dlclose(handle);
    return 0;
}
