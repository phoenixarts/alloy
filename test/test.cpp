#include <alloy.hpp>
#include <ctime>
using namespace Alloy;

time_t start;

void getTime() {
    Number time = float( clock () - start ) /  CLOCKS_PER_SEC;
    Text col = "rgb(120,255,0)";
    if (time > 1) col = "rgb(255,0,0)";
    else if (time > 0.01) col = "rgb(255,120,0)";
    else if (time > 0.001) col = "rgb(255,255,0)";
    log(color(col, Text("Elapsed @ seconds", {time.fixed(6)})));
}

void Main() {
    start = clock();
    std::cout << "Tree: " << sizeof(Tree) << " bytes.\n";

    Tree newt = Tree(
        "scopes", Mix<Text>()
    );
    newt["some"] = Number(12);
    newt["block"].join(Tree(Number(12)));

    log(Text("()0").slice(1));

    getTime();
}

MAIN(Main)
