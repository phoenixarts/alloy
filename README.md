![](images/alloy-readme.png)

# Alloy - High Level library for C++

Alloy let's you write code faster and easier yet still being fast in execution. This library is a foundation for a **Magma** programming language which environment is provided by Alloy. You can think of it like a library that provides a new cyberspace of API based on the bare metal C++. Please don't confuse Alloy with Boost library, because Alloy provides also easy solutions for different APIs such as graphics or audio.

## Prerequisities to compile Alloy

- clang (`sudo apt install clang`)
- llvm-ar (`suco apt install llvm`)
  *(note: on Windows llvm-ar comes bundled with clang. Remember to put clang and llvm-ar to PATH)*

Linux:

- **libgl-dev**

- **libx11-dev**

## Libraries to link when using the library (-l<lib_name>):

### Linux

- dl
- X11
- GL

### Windows:

- Shell32
- User32

![](images/wallpaper.png)
