#ifndef COMPILED

    // Universal C++ Libraries
    #include <iostream>
    #include <sstream>
    #include <algorithm>
    #include <functional>
    #include <vector>
    #include <map>
    #include <memory>
    #include <exception>
    #include <type_traits>
    #include <initializer_list>
    #include <cstring>
    #include <filesystem>
    #include <fstream>
    #include <cmath>
    #include <regex>
    #include <variant>
    // Universal C Libraries
    #include <stdio.h>
    #include <stdlib.h>
    // --- Global Variables ---
    // Tells whether current
    // system supports rgb colors
    static bool TERM_RGB_COL = true;
    
    // Cross platform layer
    #ifdef _WIN32
        #include <windows.h>

        // #define  _CRT_SECURE_NO_WARNINGS 1
        
        // Deque wrapper implementation
        // for windows VC compiler
        namespace std {
            template<class T>
            class deque {
            public:
                std::vector<T> value;
                deque() {}
                deque(initializer_list<T> vals) : value(vals) {}
                auto size() { return value.size(); }
                auto begin() { return value.begin(); }
                auto end() { return value.end(); }
                auto& at(size_t pos) { return value.at(pos); }
                auto& operator[](size_t pos) { return value.at(pos); }
                template<class U>
                auto erase(U pos) { return value.erase(pos); }
                auto clear() { return value.clear(); }
                auto push_back(T val) { return value.push_back(val); }
                auto pop_back() { return value.pop_back(); }
                auto push_front(T val) {
                    std::vector<T> res;
                    res.push_back(val);
                    for (const auto& item : this->value) {
                        value.push_back(item);
                    }
                }
                auto pop_front() {
                    std::vector<T> res;
                    bool start = true;
                    for (const auto& item : this->value) {
                        if (start) {
                            start = false;
                            continue;
                        }
                        value.push_back(item);
                    }
                }
            };
        }
        
        #ifdef COMPILED
        
        // Enable Bash
        // terminal coloring
        void bashColors() {
            HANDLE stdoutHandle;
            DWORD outModeInit;
            DWORD outMode = 0;
            stdoutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
        
            GetConsoleMode(stdoutHandle, &outMode);
            outModeInit = outMode;
        
            // Enable ANSI escape codes
            outMode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
        
            if(!SetConsoleMode(stdoutHandle, outMode)) {
                if (GetLastError() == 87) {
                    TERM_RGB_COL = false;
                }
                return;
            }
        }
        
        
        // Run all the windows required polyfills
        void windowsInit() {
            bashColors();
        }
        
        #else
        
        void bashColors();
        void windowsInit();
        
        #endif
        

    #else
        #include <deque>
        #include <dlfcn.h>
        #include <unistd.h>
        #include <sys/types.h>
        #include <sys/stat.h>
        #include <fcntl.h> 
        #include <math.h>
    #endif

#endif
namespace Alloy {


    #undef None
    class None {
    public:
        None(){}
    };
    #define none None()
    
    class Number;
    class Switch;
    class Text;
    
    template<class T>
    class Mix;
    
    template<class T>
    class Map;
    
    class Failure;
    
    namespace Old {
        class Tree;
    }
    namespace New {
        class Tree;
    }
    
    class Module;

    std::string color(std::string color, std::string line);

    /* Headers */


    
    class Switch {
    public:
        bool value = false;
    
        Switch();
        Switch(bool value);
    
        Switch operator ! ();
        Number operator + (Number other);
        Number operator - (Number other);
        Number operator * (Number other);
        Number operator / (Number other);
        Number operator % (Number other);
        Number operator + (Switch other);
        Number operator - (Switch other);
        Number operator * (Switch other);
        Number operator / (Switch other);
        Number operator % (Switch other);
    
        operator bool();
        operator Number();
        operator Text();
    };
    
    #define on Switch(true)
    #define off Switch(false)

    
    class Number {
    public:
        double value = 0;
    
        Number();
        Number(double value);
        Number(Text value);
        Number(Switch value);
    
        Number toInt();
        Number round();
        Number floor();
        Number ceil();
        Number trunc();
        Switch isInt();
        int cint() const;
    
        Number sum(const Number& other);
        Number substract(const Number& other);
        Number multiply(const Number& other);
        Number divide(const Number& other);
        Number modulo(const Number& other);
        Number power(const Number& other);
        Number& increment();
        Number& decrement();
        Switch gt(const Number& other);
        Switch lt(const Number& other);
        Switch gteq(const Number& other);
        Switch lteq(const Number& other);
    
        operator double();
        operator Switch();
        operator Text();
        Text fixed(const Number& precision);
    
        Number& operator += (const Number& other);
        Number operator + (const Number& other);
        Number operator - (const Number& other);
        Number operator * (const Number& other);
        Number operator / (const Number& other);
        Number operator % (const Number& other);
        Number operator + (const Switch& other);
        Number operator - (const Switch& other);
        Number operator * (const Switch& other);
        Number operator / (const Switch& other);
        Number operator % (const Switch& other);
        Number& operator ++ (int _);
        Number& operator -- (int _);
        friend Number operator+(const Number& left, int right);
        friend Number operator-(const Number& left, int right);
    };


    
    class Text {
    public:
        std::string value;
    
        Text();
        Text(const std::string& value);
        Text(const char* value);
        Text(char value);
        Text(const Text& main, std::initializer_list<Text> values);
    
        Text sum(Text other);
        Text get(Number index) const;
        Text set(Number index, Text value);
    
        Switch equals(Text value);
        Number len() const;
        Text slice(Number start, Number end = -0.1);
        Switch has(Text symbol);
        Number index(Text symbol);
        Text& reverse();
        Text& trim();
        Text& upper(Switch first = off);
        Text& lower(Switch first = off);
        Switch isNumber();
        Switch isAlpha();
        Text join(Mix<Text> mix);
        Mix<Text> split(Text separator);
        Text& width(Number length, Switch left = off);
        const char* cstr();
        
    
        operator std::string();
        operator Switch();
        
        Text operator [](Number index);
        Text operator+=(Text other);
        Switch operator==(Text other);
        Switch operator!=(Text other);
        friend Text operator+(Text left, Text right);
        friend Text operator+(const char* left, Number right);
        friend Text operator+(const char* left, Text right);
    
    };

    
    //         --- GET ---
    // Here is a set of functions
    // Which do best to get user input
    
    Text get(Text prompt);

    void put(Text val);
    void put(Number val);

    class Pipe {
    public:
        Text name;
        Text path;
        Number buffer;
    
        Pipe(Text name, Number buffer = 1);
        Text read();
        void write(Text data);
        void remove();
    };


    // Alloy main fail class
    class Failure : public std::exception {
    public:
        Number code = 0;
        Text reason = "";
        Text function = "";
    
        Failure();
        Failure(Number code, Text reason);
        Failure(Number code, Text reason, Text function);
        virtual const char* what() const throw ();
    };


    // Process class responsible for
    // information about current process
    class Process {
    public:
    
        static Mix<Text> args;
        static Text os;
        static void init(int argc, char** argv);
    
    };
    
    #ifdef _WIN32
        #define MAIN(name) \
        int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) { \
            { \
                Alloy::Process::init(__argc, __argv); \
            } \
            try { \
                name(); \
            } \
            catch(Failure fail) { \
                Alloy::Text fun = "\n"; \
                if (fail.function.len()) { \
                    fun = Alloy::Text(" in function: @()\n", {fail.function}); \
                } \
                int msgboxID = MessageBox( \
                    NULL, \
                    Text("[code: @]@@", {fail.code, fun, fail.reason}).cstr(), \
                    "Runtime Error", \
                    MB_ICONERROR | MB_OK \
                ); \
            } \
            return 0; \
        }
    #else
        #define MAIN(name) \
            int main(int argc, char** argv) { \
                Alloy::Process::init(argc, argv); \
                try { \
                    name(); \
                } \
                catch(Failure fail) { \
                    fail.what(); \
                } \
                return 0; \
            }
    #endif


    class RegExp {
    public:
        std::regex regexp;
    
        RegExp() {}
        RegExp(Text reg);
        Switch match(Text line);
        Switch search(Text line);
        Mix<Mix<Text>> groups(Text line);
    };


    class Path {
    public:
        
        static Text exec();
        static Text execDir();
        static Text file(Text path);
        static Text dir(Text path);
        static Text ext(Text path);
        static Text absolute(Text path);
        static Text relative(Text path);
        static Switch exists(Text path);
        static Text getType(Text path);
        static Text from(Text path);
        static Text join(Mix<Text> path);
        static Text cwd();
        static Mix<Text> listDir(Text path);
    
    };


    class FS {
    public:
    
        static void remove(Text path);
        static void directory(Text path);
        static void copy(Text from, Text to);
        static void move(Text from, Text to);
    
    };

    class File {
    public:
        Text path;
    
        File();
        File(Text path);
    
        void write(Text data);
        void append(Text data);
        Text read();
        Mix<Text> readLines();
        Text getPath();
        void setPath(Text path);
        void remove();
    
    };


    /* Template classes and implementations */


    // Iterate Array with index
    #define loop(array, iter) \
        for (Number iter; iter < array.len(); iter++)
    
    // Iterate Array with index and value
    #define loopWith(array, iter, val) \
        if (array.len()) \
            for (auto [iter, val] = std::tuple{Number(0), array.get(Number(0))}; iter < array.len(); iter++, val = (iter < array.len()) ? array.get(iter) : array.get(Number(0)))
    
    // Loop From To
    #define loopFromTo(iter, from, to) \
        if (from != to) \
            for (Number iter = from; (from < to) ? (iter < to) : (iter >= to) ; (from < to) ? iter++ : iter--)

    //         --- LOG ---
    // Here is a set of functions
    // Which do best to log out values
    // It's used mostly for debug purposes
    
    // template<class T>
    // void log(T val, Switch newline = on, Number indent = 0);
    
    void log(const char* value, Switch newline = on, Number indent = 0);
    
    void log(Text val, Switch newline = on, Number indent = 0);
    
    void log(Switch val, Switch newline = on, Number indent = 0);
    
    void log(Number val, Switch newline = on, Number indent = 0);
    
    template<class T>
    void log(Mix<T> arr, bool newline = on, Number indent = 0);
    
    template<class T>
    void log(Map<T> map, Switch newline = on, Number indent = 0);
    
    // // For basic C++ types
    // template<class T>
    // void log(T val, Switch newline, Number indent) {
    //     std::cout << val;
    //     if (newline) {
    //         std::cout << std::endl;
    //     }
    // }
    
    // For Mix type
    template<class T>
    void log(Mix<T> arr, bool newline, Number indent) {
        Text col = "rgb(100, 100, 100)";
        std::cout << color(col, "[");
        loop(arr, i) {
            T& item = arr[i];
            log(item, off);
            if (i != arr.len() - Number(1)) {
                std::cout << color(col, ", ");
            }
        }
        std::cout << color(col, "]");
        if (newline) std::cout << std::endl;
    }
    
    // For Map type
    template<class T>
    void log(Map<T> map, Switch newline, Number indent) {
        Switch contains = map.len();
        Text col = "rgb(100, 100, 100)";
        std::cout << color(col, "<Map> ");
        std::cout << color(col, (contains) ? "{\n" : "{ ");
        loopWith (map, iter, key) {
            auto& val = map[key];
            for (Number i = 0; i < indent; i++) std::cout << "  ";
            std::cout << "  ";
            std::cout << (std::string)key;
            std::cout << " = ";
            log(val, off, indent + Number(1));
            std::cout << "\n";
        }
        if (contains)
            for (Number i = 0; i < indent; i++) std::cout << "  ";
        std::cout << color(col, "}");
        if(newline) std::cout << std::endl;
    }
    
    // If it's a header
    // #ifndef COMPILED
    //
    //
    //     // Factory macro for any other type
    //     #define logType(type, attrs) \
    //         void log(type val, Switch newline = on, Number indent = 0);
    // #else
        // Factory macro for any other type
        #define logType(type, attrs) \
            void log(type val, Switch newline = on, Number indent = 0) { \
                Text col = "rgb(100, 100, 100)"; \
                std::cout << color(col, Text("<").sum(Text(#type)).sum("> ")); \
                std::cout << color(col, "{\n"); \
                auto logAttr = [&] (Text name, auto attr) { \
                    for (int i = 0; i < indent; i++) std::cout << "  "; \
                    std::cout << "  "; \
                    std::cout << (std::string)name; \
                    std::cout << " = "; \
                    log(attr, false, indent.sum(Number(1))); \
                    std::cout << "\n"; \
                }; \
                attrs \
                for (Number i = 0; i < indent; i++) std::cout << "  "; \
                std::cout << color(col, "}"); \
                if(newline) std::cout << std::endl; \
            }
    // #endif


    
    template<class T>
    class Mix {
    public:
        std::deque<T> value;
    
        Mix();
        Mix(std::deque<T> other);
        Mix(std::vector<T> other);
        Mix(std::initializer_list<T> other);
    
        Number len();
        T& get(Number index);
        void set(Number index, T value);
        Mix<T>& fill(T val, Number count);
        Mix<T>& join(T val);
        Mix<T>& leave();
        Mix<T>& push(T val);
        Mix<T>& pull();
        Mix<T>& reverse();
        Mix<T>& clear();
        Mix<T> sum(Mix<T> value);
        T& last();
        Switch has(T val);
        Number index(T val);
        Mix<T> slice(Number start, Number end = -0.1);
    
        T& operator[](Number index);
        Mix<T> operator+(Mix<T> value);
        Mix<T> operator+=(Mix<T> value);
    };
    
    
    // --- Header Implementation ---
    
    
    template<class T>
    Mix<T>::Mix() {}
    
    // Create a mix with deque type
    template<class T>
    Mix<T>::Mix(std::deque<T> other) {
        this->value = other;
    }
    
    // Create a mix with vector type
    template<class T>
    Mix<T>::Mix(std::vector<T> other) {
        this->value.clear();
        for (auto& item : other) {
            this->value.push_back(item);
        }
    }
    
    // Create a mix with
    // initializer list
    template<class T>
    Mix<T>::Mix(std::initializer_list<T> other) {
        this->value = std::deque<T>(other);
    }
    
    // Join a new element to
    // the end of the mix
    template<class T>
    Mix<T>& Mix<T>::join(T val) {
        this->value.push_back(val);
        return *this;
    }
    
    // Leave the last
    // element from the mix
    template<class T>
    Mix<T>& Mix<T>::leave() {
        if (this->value.size())
            this->value.pop_back();
        return *this;
    }
    
    // Push a new element to
    // the beginning of the mix
    template<class T>
    Mix<T>& Mix<T>::push(T val) {
        this->value.push_front(val);
        return *this;
    }
    
    // Pull out the first
    // element from the mix
    template<class T>
    Mix<T>& Mix<T>::pull() {
        if (this->value.size())
            this->value.pop_front();
        return *this;
    }
    
    // Reverse the mix
    template<class T>
    Mix<T>& Mix<T>::reverse() {
        std::reverse(this->value.begin(), this->value.end());
        return *this;
    }
    
    // Get length of the mix
    template<class T>
    Number Mix<T>::len() {
        return this->value.size();
    }
    
    // Get child by index
    template<class T>
    T& Mix<T>::get(Number index) {
        if (index < 0)
            throw Failure(1,
                Text(
                    "Provided index is smaller than zero in the mix \n(asked for: @, current size: @)",
                    {index, Number(this->value.size())}
                ),
                __func__
            );
        if (index >= this->value.size())
            throw Failure(2,
                Text(
                    "Provided index is higher or equal to size of the mix \n(asked for: @, current size: @)",
                    {index, Number(this->value.size())}
                ),
                __func__
            );
        return this->value[index.cint()];
    }
    
    // Set child by index
    template<class T>
    void Mix<T>::set(Number index, T value) {
        if (index < 0)
            throw Failure(1,
                Text(
                    "Provided index is smaller than zero in the mix \n(asked for: @, current size: @)",
                    {index, Number(this->value.size())}
                ),
                __func__
            );
        if (index >= this->value.size())
            throw Failure(2,
                Text(
                    "Provided index is higher or equal to size of the mix \n(asked for: @, current size: @)",
                    {index, Number(this->value.size())}
                ),
                __func__
            );
        this->value[index] = value;
    }
    
    // Determines if mix
    // contains given value
    template<class T>
    Mix<T>& Mix<T>::fill(T val, Number count) {
        this->value.clear();
        loopFromTo(i, 0, count) {
            this->join(val);
        }
        return *this;
    }
    
    // Determines if mix
    // contains given value
    template<class T>
    Number Mix<T>::index(T val) {
        loopFromTo(i, 0, this->value.size()) {
            if (this->value[i] == val) return i;
        }
        return -1;
    }
    
    // Determines if mix
    // contains given value
    template<class T>
    Switch Mix<T>::has(T val) {
        for (T& item : this->value)
                if (item == val) return on;
        return off;
    }
    
    // Remove all elements
    // from the mix
    template<class T>
    Mix<T>& Mix<T>::clear() {
        this->value.clear();
        return *this;
    }
    
    // Merge other mix
    // into this one
    // by extending it
    // on the end side
    template<class T>
    Mix<T> Mix<T>::sum(Mix<T> value) {
        Mix<T> res = this->value;
        loopWith(value, index, val) {
            res.join(val);
        }
        return res;
    }
    
    // Get the last
    // element in the mix
    template<class T>
    T& Mix<T>::last() {
        return this->get(this->len()--);
    }
    
    // Slice the mix in order to get a new
    // mix which is a submix of the mix
    template<class T>
    Mix<T> Mix<T>::slice(Number start, Number end) {
        Mix<T> mix;
        // If the slice will
        // be length of zero
        if (start == end) return mix;
        // Count from the end
        if (start < 0)
            start = this->value.size() + start;
        if (end <= 0)
            end = this->value.size() + end;
        // If index is inapropriate
        if (start < 0)
            throw Failure(1, Text("Couldn't slice mix, starting point is less than zero (asked for: @)", {start}), __func__);
        if (end > this->value.size())
            throw Failure(2, Text("Couldn't slice mix, ending point is greater than mix length (asked for: @)", {end}), __func__);
        // The slice logic
        for (int i = start; i < end; i++)
            mix.join(this->value[i]);
        return mix;
    }
    
    
    // --- C++ Operators ---
    
    
    template<class T>
    T& Mix<T>::operator[](Number index) {
        return this->get(index);
    }
    
    template<class T>
    Mix<T> Mix<T>::operator+(Mix<T> value) {
        return this->sum(value);
    }
    
    template<class T>
    Mix<T> Mix<T>::operator+=(Mix<T> value) {
        return this->value = this->sum(value).value;
    }


    
    
    template<class T>
    class Map {
    public:
        std::deque<std::pair<Text,T>> value;
    
        Map();
        Map(std::initializer_list<std::pair<Text,T>> val);
    
        Number len();
        T& get(const Text& key);
        Text get(Number index);
        T& set(const Text& key, T value);
        T& set(Number index, T value);
        Map<T>& remove(const Text& key);
        Map<T>& clear();
        Switch has(Text key);
        Mix<Text> keys();
        Mix<T> values();
    
        T& operator[](Text key);
        Text operator[](Number key);
    };
    
    // --- Header Implementation --- 
    
    
    template<class T>
    Map<T>::Map() {}
    
    
    // Initialize map with initializer list
    template<class T>
    Map<T>::Map(std::initializer_list<std::pair<Text,T>> val) {
        value.clear();
        for (auto item : val) {
            value.push_back(item);
        }
    }
    
    // Show length of the map
    template<class T>
    Number Map<T>::len() {
        return this->value.size();
    }
    
    // Get child of the map by key
    template<class T>
    T& Map<T>::get(const Text& key) {
        for (Number i = 0; i < this->value.size(); i++) {
            auto& item = this->value[i];
            if (item.first == key) {
                return item.second;
            }
        }
        throw Failure(1, Text("Map doesn't contain asked key '@'", {key}), __func__);
    }
    
    // Get child of the map by index
    template<class T>
    Text Map<T>::get(Number index) {
        if (index < 0)
            throw Failure(1,
                Text(
                    "Provided index is smaller than zero in the map \n(asked for: @, current size: @)",
                    {index, Number(this->value.size())}),
                __func__
            );
        if (index >= this->value.size())
            throw Failure(2,
                Text(
                    "Provided index is higher or equal to size of the map \n(asked for: @, current size: @)",
                    {index, Number(this->value.size())}),
                __func__
            );
        return this->value[index].first;
    }
    
    // Set child of the map by key
    template<class T>
    T& Map<T>::set(const Text& key, T value) {
        for (Number i = 0; i < this->value.size(); i++) {
            auto& item = this->value[i];
            if (item.first == key) {
                item.second = value;
                return item.second;
            }
        }
        this->value.push_back(std::make_pair(key, value));
        return this->get(key);
    }
    
    // Set child of the map by index
    template<class T>
    T& Map<T>::set(Number index, T value) {
        if (index < 0)
            throw Failure(1,
                Text(
                    "Provided index is smaller than zero in the map \n(asked for: @, current size: @)",
                    {index, Number(this->value.size())}),
                __func__
            );
        if (index >= this->value.size())
            throw Failure(2,
                Text(
                    "Provided index is higher or equal to size of the map \n(asked for: @, current size: @)",
                    {index, Number(this->value.size())}),
                __func__
            );
        return this->value[index] = value;
    }
    
    // Remove child of the map by key
    template<class T>
    Map<T>& Map<T>::remove(const Text& key) {
        for (Number i = 0; i < this->value.size(); i++) {
            auto& item = this->value[i];
            if (item.first == key) {
                this->value.erase(this->value.begin() + i);
            }
        }
        return *this;
    }
    
    // Clear this map
    template<class T>
    Map<T>& Map<T>::clear() {
        this->value.clear();
        return *this;
    }
    
    // Check if map has given key
    template<class T>
    Switch Map<T>::has(Text key) {
        for (Number i = 0; i < this->value.size(); i++) {
            if (this->value[i].first == key) return on;
        }
        return off;
    }
    
    // Return all the keys
    template<class T>
    Mix<Text> Map<T>::keys() {
        Mix<Text> res;
        for (Number i = 0; i < this->value.size(); i++) {
            res.join(this->value[i].first);
        }
        return res;
    }
    
    // Return all the values
    template<class T>
    Mix<T> Map<T>::values() {
        Mix<T> res;
        for (Number i = 0; i < this->value.size(); i++) {
            res.join(this->value[i].second);
        }
        return res;
    }
    
    
    // --- C++ Operators ---
    
    
    template<class T>
    T& Map<T>::operator [](Text key) {
        return this->get(key);
    }
    
    template<class T>
    Text Map<T>::operator [](Number index) {
        return this->get(index);
    }

    #ifdef _WIN32
    
    class Module {
    public:
        HMODULE handle = nullptr;
        Switch loaded = off;
        Text path = "";
    
        Module();
        Module(Text path);
        void open();
        template<class T>
        T load(Text name);
        void close();
    };
    
    // Laod a function
    // from the module
    template<class T>
    T Module::load(Text name) {
        if (!this->loaded) throw Failure(1, Text("Couldn't load closed module\n(@)", {this->path}));
        T function = nullptr;
        char rawname[512];
        strcpy_s(rawname, name.cstr());
        void* dllfun = GetProcAddress(this->handle, rawname);
        reinterpret_cast<void*&>(function) = dllfun;
        char* error;
        if (dllfun == NULL)  {
            throw Failure(2,
                Text(
                    "Loading function '@' from Magma Rumtime Module failed.\n",
                    {name}
                ),
                __func__
            );
        }
        return function;
    }
    
    #else
    
    class Module {
    public:
        void* handle = nullptr;
        Switch loaded = off;
        Text path = "";
    
        Module();
        Module(Text path);
        void open();
        template<class T>
        T load(Text name);
        void close();
    };
    
    // Laod a function
    // from the module
    template<class T>
    T Module::load(Text name) {
        if (!this->loaded) throw Failure(1, Text("Couldn't load closed module\n(@)", {this->path}));
        T function = nullptr;
        char rawname[512];
        std::strcpy(rawname, name.cstr());
        void* dllfun = dlsym(this->handle, rawname);
        reinterpret_cast<void*&>(function) = dllfun;
        char* error;
        if ((error = dlerror()) != NULL)  {
            throw Failure(2,
                Text(
                    "Loading function '@' from Magma Rumtime Module failed.\nMore details:\n\n@",
                    {name, dlerror()}
                ),
                __func__
            );
        }
        return function;
    }
    
    #endif


    enum TreeValue {
        NONE,
        SWITCH,
        NUMBER,
        TEXT,
        MIX,
        TREE
    };
    
    struct TreeToken {
        Text data = "";
        Number pos = 0;
        Number scope = -1;
    };
    
    class Tree {
    public:
        std::variant<Text,Number,Switch,Map<Tree>,Mix<Tree>> var;
        TreeValue type = NONE;
    
        Tree();
        
        // Inline initializer
        Tree(Switch value);
        Tree(Number value);
        Tree(Text value);
        template<typename T>
        Tree(Mix<T> value);
        template <typename T, typename... Args>
        Tree(const Text& key, const T& val, Args... args);
    
        // Tree Initializers
        void init() {}
        template <typename... Args >
        void init(const Text& key, const Switch& val, Args... args);
        template <typename... Args >
        void init(const Text& key, const Number& val, Args... args);
        template <typename... Args >
        void init(const Text& key, const Text& val, Args... args);
        template <typename T, typename... Args >
        void init(const Text& key, const Mix<T>& val, Args... args);
        template <typename... Args >
        void init(const Text& key, const Tree& val, Args... args);
    
        // Getters / Setters
        Tree& get(const Text& key);
        Tree& get(Number key);
        template<typename T>
        Tree& set(const Text& key, const T& value);
        template<typename T>
        Tree& set(const Number& key, const T& value);
        template<typename T>
        T cast();
        Mix<Tree> list();
        Map<Tree> tree();
        template<typename T>
        Mix<T> mix();
    
        // Type methods
        Text getType();
        Switch isNone();
        Switch isSwitch();
        Switch isNumber();
        Switch isText();
        Switch isMix();
        Switch isTree();
    
        // Methods
        template<typename T>
        Tree& join(const T& val);
        Tree& leave();
        template<typename T>
        Tree& push(const T& val);
        Tree& pull();
        Tree& path(Mix<Text> path);
        Number len();
        // TODO
        Tree& last();
        Switch has(const Text& key);
        Tree& remove(const Text& key);
    
        // Helper methods
        void error(const Text& message, Number pos, Text& source);
    
        // Parse Tree
        Tree& parse(Text value);
        Mix<TreeToken> lex(Text& value);
        Mix<Mix<Mix<TreeToken>>> chunk(Mix<TreeToken>& lex, Text& literal);
        Tree ast(Mix<Mix<TreeToken>> context, Mix<Mix<Mix<TreeToken>>>& chunks, Text& literal);
    
        // Dump Tree
        Text dump(Number indent = 1);
        Text dumpStyled(Number indent = 1);
    
        // Operators
        Tree& operator[](const Text& name);
        Tree& operator[](Number index);
        Tree& operator[](int index);
    
    };
    
    
    // DEBUG TREE TOKEN LOG 
    #ifdef COMPILED
        logType(TreeToken, {
            logAttr("data", val.data);
            logAttr("pos", val.pos);
            logAttr("scope", val.scope);
        })
    #else
        void log(TreeToken val, Switch newline = on, Number indent = 0);
    #endif
    
    // TREE TYPE LOG
    #ifdef COMPILED
        void log(Tree val, Switch newline = on, Number indent = 0) {
            log(val.dumpStyled());
        }
    #else
        void log(Tree val, Switch newline = on, Number indent = 0);
    #endif
    
    
    // --- Inline initializer ---
    
    
    template<typename T>
    Tree::Tree(Mix<T> value) {
        Mix<Tree> result;
        loopWith (value, index, item) {
            result.join(Tree(item));
        }
        this->var = result;
        this->type = MIX;
    }
    
    template <typename T, typename... Args>
    Tree::Tree(const Text& key, const T& val, Args... args) {
        this->init(key, val, args...);
    }
    
    template <typename... Args>
    void Tree::init(const Text& key, const Switch& val, Args... args)  {
        this->set(key, val);
        this->init(args...);
    }
    
    template <typename... Args>
    void Tree::init(const Text& key, const Number& val, Args... args)  {
        this->set(key, val);
        this->init(args...);
    }
    
    template <typename... Args>
    void Tree::init(const Text& key, const Text& val, Args... args)  {
        this->set(key, val);
        this->init(args...);
    }
    
    template <typename T, typename... Args>
    void Tree::init(const Text& key, const Mix<T>& val, Args... args)  {
        this->set(key, val);
        this->init(args...);
    }
    
    template <typename... Args>
    void Tree::init(const Text& key, const Tree& val, Args... args)  {
        this->set(key, val);
        this->init(args...);
    }
    
    // Getters / Setters
    
    template<typename T>
    Tree& Tree::set(const Text& key, const T& value) {
        if (this->type != TREE)
            this->var = Map<Tree>();
        this->type = TREE;
        Map<Tree>& ptr = std::get<Map<Tree>>(this->var);
        ptr.set(key, value);
        return *this;
    }
    
    template<typename T>
    Tree& Tree::set(const Number& key, const T& value) {
        if (this->type == TREE) {
            Map<Tree>& ptr = std::get<Map<Tree>>(this->var);
            ptr.set(key, value);
        }
        else if (this->type == MIX) {
            Mix<Tree>& ptr = std::get<Mix<Tree>>(this->var);
            ptr.set(key, value);
        }
        else 
            throw Failure(1, Text("Cannot index type '@' and return a Tree", {getType()}), __func__);
        return *this;
    }
    
    template<typename T>
    T Tree::cast() {
        if (this->type == TREE || this->type == MIX)
            throw Failure(1, Text("Cannot cast '@' to complex data type like 'tree' or 'mix'", {getType()}), __func__);
        return std::get<T>(this->var);
    }
    
    template<typename T>
    Mix<T> Tree::mix() {
        if (this->type != MIX)
            throw Failure(1, Text("Cannot cast '@' to 'mix'", {getType()}), __func__);
        Mix<T> result;
        loopWith(std::get<Mix<Tree>>(var), index, value) {
            result.join(value.cast<T>());
        }
        return result;
    }
    
    
    // --- Methods ---
    
    
    template<typename T>
    Tree& Tree::join(const T& val) {
        if (this->type != MIX)
            this->var = Mix<Tree>();
        this->type = MIX;
        Mix<Tree>& ptr = std::get<Mix<Tree>>(this->var);
        ptr.join(val);
        return *this;
    }
    
    template<typename T>
    Tree& Tree::push(const T& val) {
        if (this->type != MIX)
            this->var = Mix<Tree>();
        this->type = MIX;
        Mix<Tree>& ptr = std::get<Mix<Tree>>(this->var);
        ptr.push(val);
        return *this;
    }

    #ifndef COMPILED
    
    // TODO: Remove letter "a"
    #ifdef __linux__a
    
    namespace X11 {
        #include <X11/Xlib.h>
    }
    
    class Window {
    public:
        X11::Display *display;
        X11::Window window;
        X11::XEvent event;
        int screen;
    
        Window(Tree config) {
            Number x = (!config["x"].isNumber()) ? Number(50) : config["x"].cast<Number>();
            Number y = (!config["y"].isNumber()) ? Number(50) : config["y"].cast<Number>();
            Number width = (!config["width"].isNumber()) ? Number(640) : config["width"].cast<Number>();
            Number height = (!config["height"].isNumber()) ? Number(400) : config["height"].cast<Number>();
            Text title = (!config["title"].isText()) ? "" :config["title"].cast<Text>();
    
            display = X11::XOpenDisplay(NULL);
            if (display == NULL) {
                throw Failure(1, "Cannot open display (X11)");
            }
            screen = (((X11::_XPrivDisplay)(display))->default_screen);
            window = X11::XCreateSimpleWindow(
                display,
                /* Root Window */ ((&((X11::_XPrivDisplay)(display))->screens[screen])->root),
                x, y,
                width, height, 1,
                /* Black Pixel */ ((&((X11::_XPrivDisplay)(display))->screens[screen])->black_pixel),
                /* White Pixel */ ((&((X11::_XPrivDisplay)(display))->screens[screen])->white_pixel)
            );
            X11::XStoreName(display, window, title.cstr());
            X11::XSelectInput(display, window, /* ExposureMask */ (1L<<15) | /* KeyPressMask */ (1L<<0));
            X11::XMapWindow(display, window);
    
            log("Window's event loop is not finished yet");
            /** TMP **/
            // while (1) {
            //     X11::XNextEvent(display, &event);
            //     if (event.type == /* Expose */ (12)) {
            //         X11::XFillRectangle(
            //                     display,
            //                     window,
            //                     /* DefaultGC */ ((&((X11::_XPrivDisplay)(display))->screens[screen])->default_gc),
            //                     20, 20,
            //                     10, 10
            //                 );
            //     }
            //     if (event.type == /* KeyPress */ (2))
            //         break;
            // }
            // X11::XCloseDisplay(display);
            // log("THE END");
            /** TMP **/
    
        }
    };
    
    #endif
    
    #endif

}