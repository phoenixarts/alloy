import sys
import re
import os
args = sys.argv

if len(args) < 2:
    print('Too little CLI arguments provided')
    print('headerize.py <input_header_file> <output_header_file>')
    sys.exit(1);

data = []

def run(path, indent = 0):
    global data
    with open(path, 'r') as file:
        for line in file.readlines():
            # If Including header file from current directory
            define = re.compile('\s*#include\s+"([^"]+)"')
            match = define.match(line)
            if match:
                data.append('\n')
                run(os.path.join(os.path.dirname(path), match.groups()[0]), indent + 1)
                data.append('\n')
                continue
            data.append(('    ' * indent) + line)
            
run(args[1])

if len(args) > 2:
    with open(args[2], 'w') as file:
        file.write(''.join(data))
else:
    print(''.join(data))



