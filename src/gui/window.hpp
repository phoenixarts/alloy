#ifndef COMPILED

// TODO: Remove letter "a"
#ifdef __linux__a

namespace X11 {
    #include <X11/Xlib.h>
}

class Window {
public:
    X11::Display *display;
    X11::Window window;
    X11::XEvent event;
    int screen;

    Window(Tree config) {
        Number x = (!config["x"].isNumber()) ? Number(50) : config["x"].cast<Number>();
        Number y = (!config["y"].isNumber()) ? Number(50) : config["y"].cast<Number>();
        Number width = (!config["width"].isNumber()) ? Number(640) : config["width"].cast<Number>();
        Number height = (!config["height"].isNumber()) ? Number(400) : config["height"].cast<Number>();
        Text title = (!config["title"].isText()) ? "" :config["title"].cast<Text>();

        display = X11::XOpenDisplay(NULL);
        if (display == NULL) {
            throw Failure(1, "Cannot open display (X11)");
        }
        screen = (((X11::_XPrivDisplay)(display))->default_screen);
        window = X11::XCreateSimpleWindow(
            display,
            /* Root Window */ ((&((X11::_XPrivDisplay)(display))->screens[screen])->root),
            x, y,
            width, height, 1,
            /* Black Pixel */ ((&((X11::_XPrivDisplay)(display))->screens[screen])->black_pixel),
            /* White Pixel */ ((&((X11::_XPrivDisplay)(display))->screens[screen])->white_pixel)
        );
        X11::XStoreName(display, window, title.cstr());
        X11::XSelectInput(display, window, /* ExposureMask */ (1L<<15) | /* KeyPressMask */ (1L<<0));
        X11::XMapWindow(display, window);

        log("Window's event loop is not finished yet");
        /** TMP **/
        // while (1) {
        //     X11::XNextEvent(display, &event);
        //     if (event.type == /* Expose */ (12)) {
        //         X11::XFillRectangle(
        //                     display,
        //                     window,
        //                     /* DefaultGC */ ((&((X11::_XPrivDisplay)(display))->screens[screen])->default_gc),
        //                     20, 20,
        //                     10, 10
        //                 );
        //     }
        //     if (event.type == /* KeyPress */ (2))
        //         break;
        // }
        // X11::XCloseDisplay(display);
        // log("THE END");
        /** TMP **/

    }
};

#endif

#endif
