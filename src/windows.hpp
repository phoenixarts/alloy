// #define  _CRT_SECURE_NO_WARNINGS 1

// Deque wrapper implementation
// for windows VC compiler
namespace std {
    template<class T>
    class deque {
    public:
        std::vector<T> value;
        deque() {}
        deque(initializer_list<T> vals) : value(vals) {}
        auto size() { return value.size(); }
        auto begin() { return value.begin(); }
        auto end() { return value.end(); }
        auto& at(size_t pos) { return value.at(pos); }
        auto& operator[](size_t pos) { return value.at(pos); }
        template<class U>
        auto erase(U pos) { return value.erase(pos); }
        auto clear() { return value.clear(); }
        auto push_back(T val) { return value.push_back(val); }
        auto pop_back() { return value.pop_back(); }
        auto push_front(T val) {
            std::vector<T> res;
            res.push_back(val);
            for (const auto& item : this->value) {
                value.push_back(item);
            }
        }
        auto pop_front() {
            std::vector<T> res;
            bool start = true;
            for (const auto& item : this->value) {
                if (start) {
                    start = false;
                    continue;
                }
                value.push_back(item);
            }
        }
    };
}

#ifdef COMPILED

// Enable Bash
// terminal coloring
void bashColors() {
    HANDLE stdoutHandle;
    DWORD outModeInit;
    DWORD outMode = 0;
    stdoutHandle = GetStdHandle(STD_OUTPUT_HANDLE);

    GetConsoleMode(stdoutHandle, &outMode);
    outModeInit = outMode;

    // Enable ANSI escape codes
    outMode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;

    if(!SetConsoleMode(stdoutHandle, outMode)) {
        if (GetLastError() == 87) {
            TERM_RGB_COL = false;
        }
        return;
    }
}


// Run all the windows required polyfills
void windowsInit() {
    bashColors();
}

#else

void bashColors();
void windowsInit();

#endif

