#ifndef COMPILED
#define COMPILED

#include "predef.cpp"
#include "alloy.h"



namespace Alloy {
    
    time_t deltaTime;
    void timeStart() {
        deltaTime = clock();
    }

    void timeEnd(const char* label = "Time") {
        float time = float(clock() - deltaTime) / CLOCKS_PER_SEC;
        Text col = (time > 1 ? "red" : "green");
        std::cout << label << ": " << color(col, Text(Number(time))) << " seconds \n";
    }

    #include "misc/colorize.cpp"

    /* Logic */

    #include "primitives/switch.cpp"
    #include "primitives/number.cpp"
    #include "primitives/text.cpp"
    #include "io/log.cpp"
    #include "io/put.cpp"
    #include "io/get.cpp"
    #include "io/pipe.cpp"
    #include "misc/fail.cpp"
    #include "misc/process.cpp"
    #include "misc/regexp.cpp"
    #include "tree/tree.cpp"
    #include "fs/path.cpp"
    #include "fs/fs.cpp"
    #include "fs/file.cpp"
    #include "module/module.cpp"
}

namespace Magma = Alloy;
#endif
