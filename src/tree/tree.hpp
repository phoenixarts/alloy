enum TreeValue {
    NONE,
    SWITCH,
    NUMBER,
    TEXT,
    MIX,
    TREE
};

struct TreeToken {
    Text data = "";
    Number pos = 0;
    Number scope = -1;
};

class Tree {
public:
    std::variant<Text,Number,Switch,Map<Tree>,Mix<Tree>> var;
    TreeValue type = NONE;

    Tree();
    
    // Inline initializer
    Tree(Switch value);
    Tree(Number value);
    Tree(Text value);
    template<typename T>
    Tree(Mix<T> value);
    template <typename T, typename... Args>
    Tree(const Text& key, const T& val, Args... args);

    // Tree Initializers
    void init() {}
    template <typename... Args >
    void init(const Text& key, const Switch& val, Args... args);
    template <typename... Args >
    void init(const Text& key, const Number& val, Args... args);
    template <typename... Args >
    void init(const Text& key, const Text& val, Args... args);
    template <typename T, typename... Args >
    void init(const Text& key, const Mix<T>& val, Args... args);
    template <typename... Args >
    void init(const Text& key, const Tree& val, Args... args);

    // Getters / Setters
    Tree& get(const Text& key);
    Tree& get(Number key);
    template<typename T>
    Tree& set(const Text& key, const T& value);
    template<typename T>
    Tree& set(const Number& key, const T& value);
    template<typename T>
    T cast();
    Mix<Tree> list();
    Map<Tree> tree();
    template<typename T>
    Mix<T> mix();

    // Type methods
    Text getType();
    Switch isNone();
    Switch isSwitch();
    Switch isNumber();
    Switch isText();
    Switch isMix();
    Switch isTree();

    // Methods
    template<typename T>
    Tree& join(const T& val);
    Tree& leave();
    template<typename T>
    Tree& push(const T& val);
    Tree& pull();
    Tree& path(Mix<Text> path);
    Number len();
    // TODO
    Tree& last();
    Switch has(const Text& key);
    Tree& remove(const Text& key);

    // Helper methods
    void error(const Text& message, Number pos, Text& source);

    // Parse Tree
    Tree& parse(Text value);
    Mix<TreeToken> lex(Text& value);
    Mix<Mix<Mix<TreeToken>>> chunk(Mix<TreeToken>& lex, Text& literal);
    Tree ast(Mix<Mix<TreeToken>> context, Mix<Mix<Mix<TreeToken>>>& chunks, Text& literal);

    // Dump Tree
    Text dump(Number indent = 1);
    Text dumpStyled(Number indent = 1);

    // Operators
    Tree& operator[](const Text& name);
    Tree& operator[](Number index);
    Tree& operator[](int index);

};


// DEBUG TREE TOKEN LOG 
#ifdef COMPILED
    logType(TreeToken, {
        logAttr("data", val.data);
        logAttr("pos", val.pos);
        logAttr("scope", val.scope);
    })
#else
    void log(TreeToken val, Switch newline = on, Number indent = 0);
#endif

// TREE TYPE LOG
#ifdef COMPILED
    void log(Tree val, Switch newline = on, Number indent = 0) {
        log(val.dumpStyled());
    }
#else
    void log(Tree val, Switch newline = on, Number indent = 0);
#endif


// --- Inline initializer ---


template<typename T>
Tree::Tree(Mix<T> value) {
    Mix<Tree> result;
    loopWith (value, index, item) {
        result.join(Tree(item));
    }
    this->var = result;
    this->type = MIX;
}

template <typename T, typename... Args>
Tree::Tree(const Text& key, const T& val, Args... args) {
    this->init(key, val, args...);
}

template <typename... Args>
void Tree::init(const Text& key, const Switch& val, Args... args)  {
    this->set(key, val);
    this->init(args...);
}

template <typename... Args>
void Tree::init(const Text& key, const Number& val, Args... args)  {
    this->set(key, val);
    this->init(args...);
}

template <typename... Args>
void Tree::init(const Text& key, const Text& val, Args... args)  {
    this->set(key, val);
    this->init(args...);
}

template <typename T, typename... Args>
void Tree::init(const Text& key, const Mix<T>& val, Args... args)  {
    this->set(key, val);
    this->init(args...);
}

template <typename... Args>
void Tree::init(const Text& key, const Tree& val, Args... args)  {
    this->set(key, val);
    this->init(args...);
}

// Getters / Setters

template<typename T>
Tree& Tree::set(const Text& key, const T& value) {
    if (this->type != TREE)
        this->var = Map<Tree>();
    this->type = TREE;
    Map<Tree>& ptr = std::get<Map<Tree>>(this->var);
    ptr.set(key, value);
    return *this;
}

template<typename T>
Tree& Tree::set(const Number& key, const T& value) {
    if (this->type == TREE) {
        Map<Tree>& ptr = std::get<Map<Tree>>(this->var);
        ptr.set(key, value);
    }
    else if (this->type == MIX) {
        Mix<Tree>& ptr = std::get<Mix<Tree>>(this->var);
        ptr.set(key, value);
    }
    else 
        throw Failure(1, Text("Cannot index type '@' and return a Tree", {getType()}), __func__);
    return *this;
}

template<typename T>
T Tree::cast() {
    if (this->type == TREE || this->type == MIX)
        throw Failure(1, Text("Cannot cast '@' to complex data type like 'tree' or 'mix'", {getType()}), __func__);
    return std::get<T>(this->var);
}

template<typename T>
Mix<T> Tree::mix() {
    if (this->type != MIX)
        throw Failure(1, Text("Cannot cast '@' to 'mix'", {getType()}), __func__);
    Mix<T> result;
    loopWith(std::get<Mix<Tree>>(var), index, value) {
        result.join(value.cast<T>());
    }
    return result;
}


// --- Methods ---


template<typename T>
Tree& Tree::join(const T& val) {
    if (this->type != MIX)
        this->var = Mix<Tree>();
    this->type = MIX;
    Mix<Tree>& ptr = std::get<Mix<Tree>>(this->var);
    ptr.join(val);
    return *this;
}

template<typename T>
Tree& Tree::push(const T& val) {
    if (this->type != MIX)
        this->var = Mix<Tree>();
    this->type = MIX;
    Mix<Tree>& ptr = std::get<Mix<Tree>>(this->var);
    ptr.push(val);
    return *this;
}