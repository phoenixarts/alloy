Tree::Tree() {}

// --- Inline Initializer ---

Tree::Tree(Switch value) {
    this->var = value;
    this->type = SWITCH;
}

Tree::Tree(Number value) {
    this->var = value;
    this->type = NUMBER;
}

Tree::Tree(Text value) {
    this->var = value;
    this->type = TEXT;
}

// --- Getters / Setters ---

Tree& Tree::get(const Text& key) {
    if (this->type == NONE) {
        this->type = TREE;
        this->var = Map<Tree>();
    }
    else if (this->type != TREE) {
        throw Failure(1, Text("Cannot get child field of type @", {type}), __func__);
    }
    Map<Tree>& ptr = std::get<Map<Tree>>(this->var);
    if (!ptr.has(key)) {
        ptr.set(key, Tree());
    }
    return ptr.get(key);
}

Tree& Tree::get(Number key) {
    if (this->type == TREE) {
        Map<Tree>& ptr = std::get<Map<Tree>>(this->var);
        return ptr.get(ptr.get(key));
    }
    else if (this->type == MIX) {
        Mix<Tree>& ptr = std::get<Mix<Tree>>(this->var);
        return ptr.get(key);
    }
    else 
        throw Failure(1, Text("Cannot index type '@' and return a Tree", {getType()}), __func__);
    return *this;
}

Text Tree::getType() {
    switch (type) {
        case NONE:
        return "none";
        case SWITCH:
        return "switch";
        case NUMBER:
        return "number";
        case TEXT:
        return "text";
        case MIX:
        return "mix";
        case TREE:
        return "tree";
        default:
        return "---";
    } 
}

Mix<Tree> Tree::list() {
    if (this->type != MIX)
        throw Failure(1, Text("Cannot cast data type '@' to 'mix'", {getType()}), __func__);
    return std::get<Mix<Tree>>(this->var);
}

Map<Tree> Tree::tree() {
    if (this->type != TREE)
        throw Failure(1, Text("Cannot cast data type '@' to 'tree'", {getType()}), __func__);
    return std::get<Map<Tree>>(this->var);
}


// --- Type methods ---


Switch Tree::isNone() {
    return type == NONE;
}

Switch Tree::isSwitch() {
    return type == SWITCH;
}

Switch Tree::isNumber() {
    return type == NUMBER;
}

Switch Tree::isText() {
    return type == TEXT;
}

Switch Tree::isMix() {
    return type == MIX;
}

Switch Tree::isTree() {
    return type == TREE;
}


// --- Helper methods ---


void Tree::error(const Text& message, Number pos, Text& source) {
    const Number size = 50;
    Number len = source.len();
    Text beginDots = "";
    Text endDots = "";
    Number beginOffset = 0;
    Number endOffset = len;
    if (pos.gt(size)) {
        beginOffset = pos - size;
        beginDots = "...";
    }
    if (pos < len - size) {
        endOffset = pos + size;
        endDots = "...";
    }
    Text before = color("red", beginDots + source.slice(beginOffset,pos));
    Text current = color("b:black:red", source[pos]);
    Text after = color("red", source.slice(pos+Number(1),endOffset) + endDots);
    throw Failure(1, 
        Text(color("b:red", "Parsing Tree failed\n")) +
        color("red", message.value) + 
        color("red", "\nAt character: " + Text(pos)) +
        "\n\n" + 
        before + current + after
    );
}


// --- Methods ---


Tree& Tree::path(Mix<Text> path) {
    Tree* ptr = this;
    if (path.len()) {
        loopWith(path, index, value) {
            ptr = &(ptr->get(value));
            if (ptr->isNone()) {
                (*ptr) = Tree();
            }
        }
    }
    return (*ptr);
}

Tree& Tree::leave() {
    if (this->type != MIX)
        this->var = Mix<Tree>();
    this->type = MIX;
    Mix<Tree>& ptr = std::get<Mix<Tree>>(this->var);
    ptr.leave();
    return *this;
}

Tree& Tree::pull() {
    if (this->type != MIX)
        this->var = Mix<Tree>();
    this->type = MIX;
    Mix<Tree>& ptr = std::get<Mix<Tree>>(this->var);
    ptr.pull();
    return *this;
}

Number Tree::len() {
    if (this->type == MIX) {
        Mix<Tree>& ptr = std::get<Mix<Tree>>(this->var);
        return ptr.len();
    }
    else 
    if (this ->type == TREE) {
        Map<Tree>& ptr = std::get<Map<Tree>>(this->var);
        return ptr.len();
    }
    else
        throw Failure(1, Text("Cannot get a length of a tree value of type '@'", {getType()}), __func__);
}

Tree& Tree::last() {
    if (this->type == MIX) {
        Mix<Tree>& ptr = std::get<Mix<Tree>>(this->var);
        return ptr.last();
    }
    else
        throw Failure(1, Text("Cannot get last item of a tree value of type '@'", {getType()}), __func__);
}

Switch Tree::has(const Text& key) {
    if (this->type == TREE) {
        Map<Tree>& ptr = std::get<Map<Tree>>(this->var);
        return ptr.has(key);
    }
    else
        throw Failure(1, Text("tree value of type '@' is not a tree", {getType()}), __func__);
}

Tree& Tree::remove(const Text& key) {
    if (this->type == TREE) {
        Map<Tree>& ptr = std::get<Map<Tree>>(this->var);
        ptr.remove(key);
        return *this;
    }
    else
        throw Failure(1, Text("tree value of type '@' is not a tree", {getType()}), __func__);
}


// --- Parse Tree ---


Tree& Tree::parse(Text literal) {
    auto lex = this->lex(literal);
    auto chunk = this->chunk(lex, literal);
    *this = this->ast(chunk[0], chunk, literal);
    return *this;
}

Mix<TreeToken> Tree::lex(Text& literal) {
    Mix<TreeToken> line = {};
    Text word = "";
    Switch text = off;
    Switch escape = off;
    // Comments
    Switch single = off;
    Switch multi = off;

    // Add new word to line
    // (and possibly add a new symbol)
    auto addWord = [&] (Number index, Text sym = " ") {
        if (sym != " " || text) word += sym;
        if (text) return;
        Number len = word.len();
        if (len)
            line.join(TreeToken {
                .data = word,
                .pos = index - len + Number((len > 1) ? 0 : 1)
            });
        word = "";
    };

    Text prev = "";
    Text next = "";

    // Main loop
    loop (literal, i) {
        auto sym = literal[i];
        next = (i >= literal.len() + Number(1)) ? "" : literal[i+Number(1)];
        prev = (i <= 0) ? "" : literal[i-Number(1)];

        /* --- Comments --- */

        // Exit single line comment
        if (sym == "\n" && single) {
            single = off;
        }

        // Exit multiline comment
        if (prev == "*" && sym == "/" && multi) {
            multi = off;
            continue;
        }

        // If it's a comment
        if (single || multi) {
            continue;
        }

        // Enter single line comment
        if (sym == "/" && next == "/" && !text && !escape && !multi) {
            single = on;
            continue;
        }

        // Enter multi line comment
        if (sym == "/" && next == "*" && !text && !escape && !single) {
            multi = on;
            continue;
        }

        /* --- Other --- */

        // Skip spaces
        if (sym == " ") {
            addWord(i);
            continue;
        }

        // Special characters
        if (Text("[]{}\n=,").has(sym) && !text) {
            addWord(i);
            addWord(i, sym);
            continue;
        }

        // Text field
        if (sym == "'" && !escape) {
            text = !text;
            addWord(i, sym);
            continue;
        }

        // Escape symbol
        if (sym == "\\" && !escape) {
            escape = on;
            continue;
        }

        // Text string
        if (text) {
            // Apostrophy
            if (sym == "'" && escape) {
                word += "'";
                escape = off;
                continue;
            }
            // Newline
            else if (sym == "n" && escape) {
                word += "\n";
                escape = off;
                continue;
            }
            // Indent
            else if (sym == "t" && escape) {
                word += "\t";
                escape = off;
                continue;
            }
            else if (escape) {
                word += "\\";
            }
        }

        // Regular symbol
        word += sym;
        escape = off;
    }
    addWord(literal.len());
    if (text) {
        this->error("Text value has been left open", literal.len()-Number(1), literal);
    }
    return line;
}

Mix<Mix<Mix<TreeToken>>> Tree::chunk(Mix<TreeToken>& lex, Text& literal) {
    Mix<Mix<Mix<TreeToken>>> chunks = {{}};
    Mix<TreeToken> line = {};
    Mix<Number> stack = {0};
    // Says if it's a tree scope
    // or is it a mix scope
    Mix<Text> layers = {"tree"};
    // Add new line to chunk
    // (and possibly add a new token)
    auto addLine = [&] (TreeToken token = TreeToken{.data = ""}, Switch flush = on) {
        if (token.data.len()) line.join(token);
        if (line.len()) chunks[stack.last()].join(line);
        line.clear();
    };
    loopWith (lex, iter, token) {
        // Add Line
        if (token.data == "\n") {
            if (!line.len()) continue;
            if (layers.last() == "mix") continue;
            addLine();
            continue;
        }
        if (token.data == "[") {
            addLine( TreeToken {
                .data = "[]",
                .pos = token.pos,
                .scope = chunks.len()
            }, off);
            stack.join(chunks.len());
            chunks.join({});
            layers.join("mix");
            continue;
        }
        if (token.data == "]") {
            if (layers.last() == "tree")
                this->error("Cannot close mix while being in tree scope", token.pos, literal);
            addLine();
            stack.leave();
            layers.leave();
            // Structure error
            if (!stack.len())
                this->error("There are no scopes left to close", token.pos, literal);
            if (layers.last() == "mix") {
                line = chunks[stack.last()].last();
                chunks[stack.last()].leave();
            }
            continue;
        }

        // Create a new scope
        if (token.data == "{") {
            addLine( TreeToken {
                .data = "{}",
                .pos = token.pos,
                .scope = chunks.len()
            }, off);
            stack.join(chunks.len());
            chunks.join({});
            layers.join("tree");
            continue;
        }
        // Close this scope
        if (token.data == "}") {
            if (layers.last() == "mix")
                this->error("Cannot close tree while being in mix scope", token.pos, literal);
            addLine();
            stack.leave();
            layers.leave();
            // Structure error
            if (!stack.len())
                this->error("There are no scopes left to close", token.pos, literal);
            if (layers.last() == "mix") {
                line = chunks[stack.last()].last();
                chunks[stack.last()].leave();
            }
            continue;
        }
        line.join(token);
    }
    addLine();
    // Structure left open error
    if (stack.len() > 1) 
        this->error(Text("@ scope(s) left unclosed", {stack.len() -Number(1)}), literal.len() -Number(1), literal);
    // mix left open error
    if (layers.last() == "mix")
        this->error("Mix has been left unclosed", literal.len() -Number(1), literal);
    return chunks;
}

Tree Tree::ast(Mix<Mix<TreeToken>> context, Mix<Mix<Mix<TreeToken>>>& chunks, Text& literal) {
    Mix<Text> nameIds = {"_", "-", "$", "@"};
    Tree result;

    // -- Complex --

    // Tree case
    if (context[0][0].scope > 0) {
        if (context[0][0].data == "{}") {
            Number scope = context[0][0].scope;
            Text name = "";
            loopWith (chunks[scope], i, line) {
                if (line.len() < 3)
                    this->error("Expression too short", line[0].pos, literal);
                // Field name
                if (line[0].data[0].isAlpha() || nameIds.has(line[0].data[0]))
                    name = line[0].data;
                else
                    this->error(Text("Name must start with a letter"
                                " or one of the following symbols: @", 
                                {Text("").join(nameIds)}),
                                line[0].pos,
                                literal
                    );
                // Equal Sign
                if (line[1].data != "=")
                    this->error("Equal sign expected",
                                line[1].pos,
                                literal
                    );
                // Value
                result[name] = this->ast(Mix({line.slice(2)}), chunks, literal);
            }
        }
        // Mix case
        else if (context[0][0].data == "[]") {
            result = Mix<Tree>();
            Number scope = context[0][0].scope;
            loopWith(chunks[scope][0], i, item) {
                if (item.data == ",") continue;
                result.join(
                    this->ast(Mix({Mix({item})}), chunks, literal)
                );
            }
        }
    }

    // -- Primitives --

    // Number Case
    else if (context[0][0].data.isNumber())
        result = Number(context[0][0].data);
    // Text Case
    else if (context[0][0].data[0] == "'")
        result = context[0][0].data.slice(1, -1);
    // Switch Case
    else if (context[0][0].data == "on")
        result = on;
    else if (context[0][0].data == "off")
        result = off;
    // None Case
    else if (context[0][0].data == "none")
        return result;
    // No Match Case
    else
        this->error("Undefined syntax", context[0][0].pos, literal);
    return result;
}


// --- Dump Tree ---


Text Tree::dump(Number indent) {
    switch (this->type) {
        case NONE: {
            return "none";
        } break;
        case SWITCH: {
            Switch& ptr = std::get<Switch>(this->var);
            return (ptr ? "on" : "off");
        } break;
        case NUMBER: {
            Number& ptr = std::get<Number>(this->var);
            return Text(ptr);
        } break;
        case TEXT: {
            Text& ptr = std::get<Text>(this->var);
            Text results = "";
            loopWith (ptr, i, sym) {
                if (sym == "'")
                    results += "\\'";
                else if (sym == "\n")
                    results += "\\n";
                else if (sym == "\t")
                    results += "\\t";
                else
                    results += sym;
            }
            return Text("'@'", {results});
        } break;
        case MIX: {
            Mix<Tree>& ptr = std::get<Mix<Tree>>(this->var);
            Mix<Text> results;
            loopWith (ptr, i, value) {
                results.join(value.dump(indent));
            }
            return Text("[@]", {Text(", ").join(results)});
        } break;
        case TREE: {
            Map<Tree>& ptr = std::get<Map<Tree>>(this->var);
            Text results = "";
            Text indentValue = "";
            loopFromTo (i, 0, indent-Number(1)) indentValue += "  ";
            loopWith (ptr, i, key) {
                results += indentValue + "  ";
                results += key;
                results += " = ";
                results += ptr[key].dump(indent +Number(1));
                results += "\n";
            }
            return Text("{\n@}", {results + indentValue});
        } break;
    }
    return "";
}

Text Tree::dumpStyled(Number indent) {
    switch (this->type) {
        case NONE: {
            return color("rgb(255,110,30)", "none");
        } break;
        case SWITCH: {
            Switch& ptr = std::get<Switch>(this->var);
            return color("rgb(255,110,30)", (ptr ? "on" : "off"));
        } break;
        case NUMBER: {
            Number& ptr = std::get<Number>(this->var);
            return color("rgb(255,150,0)", Text(ptr));
        } break;
        case TEXT: {
            Text& ptr = std::get<Text>(this->var);
            Text results = "";
            loopWith (ptr, i, sym) {
                if (sym == "'")
                    results += "\\'";
                else if (sym == "\n")
                    results += "\\n";
                else if (sym == "\t")
                    results += "\\t";
                else
                    results += sym;
            }
            return color("rgb(180,200,50)", Text("'@'", {results}));
        } break;
        case MIX: {
            Mix<Tree>& ptr = std::get<Mix<Tree>>(this->var);
            Mix<Text> results;
            loopWith (ptr, i, value) {
                results.join(value.dumpStyled(indent));
            }
            return color("rgb(100,100,100)", "[") 
                   + Text(color("rgb(100,100,100)", ", ")).join(results) 
                   + color("rgb(100,100,100)", "]");
        } break;
        case TREE: {
            Map<Tree>& ptr = std::get<Map<Tree>>(this->var);
            Text results = "";
            Text indentValue = "";
            loopFromTo (i, 0, indent-Number(1)) indentValue += "  ";
            loopWith (ptr, i, key) {
                results += indentValue + "  ";
                results += key;
                results += color("rgb(100,100,100)", " = ");
                results += ptr[key].dumpStyled(indent +Number(1));
                results += "\n";
            }
            return color("rgb(100,100,100)", "{\n") 
                   + results + indentValue
                   + color("rgb(100,100,100)", "}");
        } break;
    }
    return "";
}


// --- Operators ---


Tree& Tree::operator[](const Text& name) {
    return this->get(name);
}

Tree& Tree::operator[](Number index) {
    return this->get(index);
}

Tree& Tree::operator[](int index) {
    return this->get(Number(index));
}