// Tree Constructor
Tree::Tree(Text literal) {
    this->parse(literal);
}

// Tree Constructor
Tree::Tree(const char* literal) {
    this->parse(Text(literal));
}

Tree::Tree(Branch obj) {
    this->type = obj.type;
    this->name = obj.name;
    this->value = obj.value;
    this->branches = obj.branches;
    this->array = obj.array;
}

// Parse tree literal
Tree& Tree::parse(Text literal) {
    auto lex = this->lex(literal);
    if (this->errMsg.len()) return *this;
    auto chunk = this->chunk(lex);
    if (this->errMsg.len()) return *this;
    auto tree = this->parseTree(0, chunk);
    if (this->errMsg.len()) return *this;

    this->branches = tree.branches;
    return *this;
}

// Checks whether tree parsage failed or not
Switch Tree::failed() {
    if (this->errMsg.len()) return on;
    return off;
}

// Send error to the user
void Tree::error(Text msg, Mix<Text> line) {
    this->errMsg = msg;
    this->errLine = Text(" ").join(line);
    if (this->throwErrors) {
        throw Failure(1, Text("Tree Error... @\nMore details:\n@", {this->errMsg, this->errLine}));
    }
}

// Parses literal creating a lex
// which is a set of tokens
Mix<Text> Tree::lex(Text literal) {
    Mix<Text> line = {};
    Text word = "";
    Switch text = off;
    Switch escape = off;
    // Comments
    Switch single = off;
    Switch multi = off;

    // Add new word to line
    // (and possibly add a new symbol)
    auto addWord = [&] (Text sym = " ") {
        if (sym != " " || text) word += sym;
        if (text) return;
        if (word.len())
            line.join(word);
        word = "";
    };

    Text prev = "";
    Text next = "";

    // Main loop
    loop (literal, i) {
        auto sym = literal[i];
        next = (i >= literal.len() + Number(1)) ? "" : literal[i+Number(1)];
        prev = (i <= 0) ? "" : literal[i-Number(1)];

        /* --- Comments --- */

        // Exit single line comment
        if (sym == "\n" && single) {
            single = off;
        }

        // Exit multiline comment
        if (prev == "*" && sym == "/" && multi) {
            multi = off;
            continue;
        }

        // If it's a comment
        if (single || multi) {
            continue;
        }

        // Enter single line comment
        if (sym == "/" && next == "/" && !text && !escape && !multi) {
            single = on;
            continue;
        }

        // Enter multi line comment
        if (sym == "/" && next == "*" && !text && !escape && !single) {
            multi = on;
            continue;
        }

        /* --- Other --- */

        // Skip spaces
        if (sym == " ") {
            addWord();
            continue;
        }
        // Special characters
        if (Text("[]{}\n=,").has(sym) && !text) {
            addWord();
            addWord(sym);
            continue;
        }
        // Text field
        if (sym == "'" && !escape) {
            text = (text) ? off : on;
            addWord(sym);
            continue;
        }
        if (sym == "\\") {
            escape = on;
            word += sym;
            continue;
        }
        // Regular symbol
        word += sym;
        escape = off;
    }
    addWord();
    if (text) {
        Mix<Text> last = {};
        loop (line, i) {
            auto& word = line[i];
            if (word == "\n") break;
            last.join(word);
        }
        this->error("Unclosed text value", last);
    }
    return line;
}

// Chunks tokens to scopes
Mix<Mix<Mix<Text>>> Tree::chunk(Mix<Text> lex) {
    Mix<Mix<Mix<Text>>> chunks = {{}};
    Mix<Text> line = {};
    Mix<Number> stack = {0};
    // Says if it's a tree scope
    // or is it a list scope
    Mix<Text> layers = {"tree"};
    // Add new line to chunk
    // (and possibly add a new token)
    auto addLine = [&] (Text token = "", Switch flush = on) {
        if (token.len()) line.join(token);
        if (line.len()) chunks[stack.last()].join(line);
        line.clear();
    };
    loopWith (lex, iter, token) {
        // Add Line
        if (token == "\n") {
            if (!line.len()) continue;
            if (layers.last() == "list") continue;
            addLine();
            continue;
        }
        if (token == "[") layers.join("list");
        if (token == "]") layers.leave();

        // Create a new scope
        if (token == "{") {
            addLine("{"+Number(chunks.len())+"}", off);
            stack.join(chunks.len());
            chunks.join({});
            layers.join("tree");
            continue;
        }
        // Close this scope
        if (token == "}") {
            addLine();
            stack.leave();
            layers.leave();
            // Structure error
            if (!stack.len()) {
                this->error("Structure closed too soon", line);
                return chunks;
            }
            if (layers.last() == "list") {
                line = chunks[stack.last()].last();
                chunks[stack.last()].leave();
            }
            continue;
        }
        line.join(token);
    }
    addLine();
    // Structure error
    if (!stack.len()) {
        this->error("Structure didn't close properly", line);
        return chunks;
    }
    return chunks;
}

// Retrieves a value of the expression
Tree Tree::parseValue(Mix<Text> val, Mix<Mix<Mix<Text>>>& chunks) {
    // Primitive data types
    if (val.len() == 1) {
        // Text value case
        if (val[0][0] == "'") {
            Text value = this->parseText(val[0], on, on);
            return Branch {
                .type = TreeTypes::TEXT,
                .value = Text("'@'", {value})
            };
        }
        // Switch value case
        else if (Mix<Text>({"on", "off"}).has(val[0])) {
            return Branch {
                .type = TreeTypes::SWITCH,
                .value = val[0]
            };
        }
        // None value case
        else if (val[0] == "none") {
            return Branch {
                .type = TreeTypes::NONE,
                .value = val[0]
            };
        }
        // Number value case
        else if (Text(val[0]).isNumber()) {
            return Branch {
                .type = TreeTypes::NUMBER,
                .value = val[0]
            };
        }
        // Tree value case
        else if (val[0][0] == "{") {
            Number index = Number(Text(val[0]).slice(1, -1));
            Tree data = this->parseTree(index, chunks);
            return data;
        }
    }
    // More complex data types
    else if (val.len() > 1) {
        // List value case
        if(val[0] == "[" && val.last() == "]") {
            Mix<Tree> items = {};
            Mix<Text> vals = val.slice(1, -1);
            Mix<Mix<Text>> children = {};
            Mix<Text> child = {};
            Number scope = 0;
            // Create array of values
            loopWith (vals, iter, item) {
                if (item == "[") scope++;
                if (Mix<Text>({"\n", ","}).has(item) && !scope) {
                    if (child.len()) {
                        children.join(child);
                        child.clear();
                    }
                }
                else {
                    child.join(item);
                }
                if (item == "]") scope--;
            }
            // Add the last child that might have left
            if (child.len()) children.join(child);
            // Evaluate each value and add to the array
            loopWith (children, iter, item) {
                items.join(this->parseValue({ item }, chunks));
            }
            return Branch {
                .type = TreeTypes::MIX,
                .array = items
            };
        }
    }
    this->error("Bad value", val);
    return {};
}

// Parses text to be easily visible to user
Text Tree::parseText(Text value, Switch slice, Switch rawText) {
    if (slice)
        value = value.slice(1, -1);
    Text newval = "";
    // Show the escaped symbols
    loopWith(value, i, sym) {
        // Newline parser
        if (sym == "\n") {
            newval += "\\n";
            continue;
        }
        // Tab parser
        if (sym == "\t") {
            newval += "\\t";
            continue;
        }
        // Text parser
        if (sym == "'" && !rawText) {
            newval += "\\'";
            continue;
        }
        // Symbol add
        newval += sym;
    }
    return newval;
}

// Validates field name
Switch Tree::validate(Text name) {
    loopWith(name, iter, sym) {
        if (sym.isAlpha()) continue;
        else if (sym.isNumber()) continue;
        else if (Text("$_").has(sym)) continue;
        else return off;

    }
    return on;
}


// Parses chunks to ready tree
Tree Tree::parseTree(Number index, Mix<Mix<Mix<Text>>>& chunks) {
    Branch base = {
        .type = TreeTypes::TREE,
        .branches = {}
    };
    Mix<Mix<Text>>& lines = chunks[index];
    loopWith (lines, iter, line) {
        if (line.len() >= 3) {
            // Bad field name
            if (!this->validate(line[0])) {
                this->error("Bad field name", line);
                return base;
            }
            // No equal sign
            if (line[1] != "=") {
                this->error("No Assignment", line);
                return base;
            }

            Tree item = this->parseValue(line.slice(2), chunks);
            item.name = line[0];
            base.branches.set(line[0], item);
        }
        else {
            this->error("Expression too short", line);
            return base;
        }
    }
    return base;
}

// --- BRANCH ---

// Get value's type name
Text Tree::getType() {
    if (((int)this->type) == TreeTypes::NONE) return "none";
    if (((int)this->type) == TreeTypes::SWITCH) return "switch";
    if (((int)this->type) == TreeTypes::NUMBER) return "number";
    if (((int)this->type) == TreeTypes::TEXT) return "text";
    if (((int)this->type) == TreeTypes::MIX) return "list";
    if (((int)this->type) == TreeTypes::TREE) return "tree";
    return "---";
}

// Determine if the value is of type none
Switch Tree::isNone() {
    return ((int)this->type) == TreeTypes::NONE;
}
// Determine if the value is of type switch
Switch Tree::isSwitch() {
    return ((int)this->type) == TreeTypes::SWITCH;
}
// Determine if the value is of type number
Switch Tree::isNumber() {
    return ((int)this->type) == TreeTypes::NUMBER;
}
// Determine if the value is of type text
Switch Tree::isText() {
    return ((int)this->type) == TreeTypes::TEXT;
}
// Determine if the value is of type list
Switch Tree::isMix() {
    return ((int)this->type) == TreeTypes::MIX;
}
// Determine if the value is of type tree
Switch Tree::isTree() {
    return ((int)this->type) == TreeTypes::TREE;
}

// Get the array from the value
// (it only makes sense if
// it's a list)
Mix<Tree>& Tree::list() {
    return this->array;
}

// Get the map from the value
// (it only makes sense if
// it's a tree)
Map<Tree>& Tree::tree() {
    return this->branches;
}


// Get key of this branch
Text Tree::key() {
    return this->name;
}

// Dump current branch to a text
Text Tree::dump() {
    return this->dumpValue(*this, 0).trim();
}

// Dump current branch to a text
void Tree::log() {
    std::cout << (std::string)this->dumpValue(*this, 0, on).trim() << std::endl;
}


// Dump the value
Text Tree::dumpValue(Tree& val, Number indent, Switch style) {
    Text res = "";
    if (val.getType() == "text") {
        res = val.style(val.value, "rgb(180,200,50)", style);
    }
    if (val.getType() == "number") {
        res = val.style(val.value, "rgb(255,150,0)", style);
    }
    if (Mix<Text>({"none", "switch"}).has(val.getType())) {
        res = val.style(val.value, "rgb(255,110,30)", style);
    }
    if (val.getType() == "list") {
        res += val.style("[", "rgb(100,100,100)", style);
        Switch childTree = off;
        // Check if it's an array of trees
        // in order to get better result
        if (val.array.len()) {
            if (val.array[0].getType() == "tree")
                childTree = on;
        }
        loop (val.array, index) {
            // Decorate opening
            if (childTree) {
                res += "\n";
                loopFromTo (i, 0, indent + Number(1)) res += "  ";
            }
            Text newval = val.dumpValue(val.array[index], indent + childTree, style);
            res += newval;
            // Non-tree separator
            if (index != val.array.len() - Number(1)) {
                res += val.style(", ", "rgb(100,100,100)", style);
            }
        }
        // Decorate closing
        if (childTree) {
            res += "\n";
            loopFromTo (i, 0, indent) res += "  ";
        }
        res += val.style("]", "rgb(100,100,100)", style);
    }
    if (val.getType() == "tree") {
        if (indent != 0 || style)
            if (!val.branches.len()) return val.style("{ }", "rgb(100,100,100)", style);
        if (indent != 0 || style)
            res += val.style("{\n", "rgb(100,100,100)", style);
        res += val.dumpTree(val.branches, indent + Number(1), style);
        loopFromTo (i, 0, indent) res += "  ";
        if (indent != 0 || style)
            res += val.style("}", "rgb(100,100,100)", style);
    }
    return res;
}

// Determine if the output shall be styled or not
Text Tree::style(Text val, Text col, Switch style) {
    if (style) {
        return color(col, val);
    }
    return val;
}

// Dump a branch object to a string
Text Tree::dumpTree(Map<Tree>& val, Number indent, Switch style) {
    Text res = "";
    if (style) {
        std::sort(val.value.begin(), val.value.end(), Tree::compareAlpha);
    }
    loopWith(val, i, key) {
        auto value = val[key];
        loopFromTo (i, 0, indent) res += "  ";
        res += key + this->style(" = ", "rgb(100,100,100)", style);
        res += this->dumpValue(value, indent, style);
        res += "\n";
    }
    return res;
}

// Helper method for sorting elements
bool Tree::compareAlpha(std::pair<Text,Tree> a, std::pair<Text,Tree> b) {
    return (std::string)a.first.value < (std::string)b.first.value;
}

// Remove a field value by key
void Tree::remove(Text key) {
    this->branches.remove(key);
}

// Get size of list or tree
Number Tree::len() {
    if (((int)this->type) == TreeTypes::MIX) {
        return this->array.len();
    }
    else {
        return this->branches.len();
    }
}

// Get last element from list
Tree& Tree::last() {
    return this->get(this->array.len()--);
}

// Pop value from a list
void Tree::leave() {
    this->array.leave();
}

// Get value from a tree and set it to be TREE if it's none
Tree& Tree::path(Mix<Text> path) {
    Tree* ptr = this;
    if (path.len()) {
        loopWith(path, index, value) {
            ptr = &(ptr->get(value));
            if (ptr->isNone()) {
                (*ptr) = Tree();
            }
        }
    }
    return (*ptr);
}

// Get value from a tree
Tree& Tree::get(Text name) {
    if (this->type.cint() != TreeTypes::TREE) {
        throw Failure(1, Text("Cannot access child field '@' when parent field's type is '@'", {name, this->getType()}), __func__);
    }
    if (!this->branches.has(name)) {
        this->branches.set(name, Branch {
            .type = TreeTypes::NONE,
            .name = name,
            .value = "none"
        });
    }
    return this->branches[name];
}

// Get value from an array / tree
Tree& Tree::get(Number index) {
    Text id = index;
    if ((int)this->type == TreeTypes::MIX) {
        if (this->array.len() < index + Number(1) || index < 0) {
            this->error("List index out of range! :" + id, {id});
        }
        return this->array[index];
    }
    else {
        if (this->branches.len() < index + Number(1) || index < 0) {
            this->error("Tree index out of range! :" + id, {id});
        }
        return this->branches[this->branches[index]];
    }
}

// Get value from a tree (operator)
Tree& Tree::operator[](Text name) {
    return this->get(name);
}

// Get value from a list or tree (operator)
Tree& Tree::operator[](Number name) {
    return this->get(name);
}

// Get value from a list or tree (operator)
Tree& Tree::operator[](int name) {
    return this->get(Number(name));
}

// Assign none (nullptr) value to the field
void Tree::operator=(None val) {
    this->type = TreeTypes::NONE;
    this->value = "none";
    this->branches.clear();
    this->array.clear();
}

// Assign switch (bool) value to the field
void Tree::operator=(Switch val) {
    this->type = TreeTypes::SWITCH;
    this->value = (val) ? "on" : "off";
    this->branches.clear();
    this->array.clear();
}

// Assign number (double) value to the field
void Tree::operator=(Number val) {
    this->type = TreeTypes::NUMBER;
    this->value = val;
    this->branches.clear();
    this->array.clear();
}

// Assign number (int) value to the field
void Tree::operator=(int val) {
    this->type = TreeTypes::NUMBER;
    this->value = val;
    this->branches.clear();
    this->array.clear();
}

// Assign text (string) value to the field
void Tree::operator=(Text val) {
    Text value = this->parseText(val);
    this->type = TreeTypes::TEXT;
    this->value = Text("'@'", {value});
    this->branches.clear();
    this->array.clear();
}

// Assign text (const char*) value to the field
void Tree::operator=(const char* val) {
    Text value = this->parseText(val);
    this->type = TreeTypes::TEXT;
    this->value = Text("'@'", {value});
    this->branches.clear();
    this->array.clear();
}

// Assign tree (Tree) value to the field
void Tree::operator=(Tree val) {
    this->type = val.type;
    this->value = val.value;
    this->branches = val.branches;
    this->array = val.array;
}
