// Enum of types
enum TreeTypes {
    NONE = 0,
    NUMBER,
    SWITCH,
    TEXT,
    MIX,
    TREE
};

// Tree interface
struct Branch {
    Number type = TreeTypes::TREE;
    Text name;
    Text value;
    Map<Old::Tree> branches;
    Mix<Old::Tree> array;
};

// Tree Class Type
class Tree {
public:
    // Internal data
    Number type = TreeTypes::TREE;
    Text name;
    Text value;
    Map<Tree> branches;
    Mix<Tree> array;
    // Error data
    Text errMsg = "";
    Text errLine = "";

    // Public data
    Switch throwErrors = on;

    // Initializers
    Tree() {}
    Tree(Branch obj);
    Tree(Text literal);
    Tree(const char* literal);
    template <typename T, typename... Args>
    Tree(Text key, T val, Args... args);
    template <typename... Args >
    void init(Text key, Switch val, Args... args);
    template <typename... Args >
    void init(Text key, Number val, Args... args);
    template <typename... Args >
    void init(Text key, Text val, Args... args);
    template <typename... Args>
    void init(const char* key, const char* val, Args... args);
    template <typename T, typename... Args>
    void init(Text key, Mix<T> val, Args... args);
    template <typename... Args>
    void init(Text key, Tree val, Args... args);
    void init() {}

    // Helper methods
    Switch failed();
    void error(Text msg, Mix<Text> line);
    Tree& parse(Text val);


    // Types and casting
    template<class T>
    T cast();
    Text getType();
    Switch isNone();
    Switch isSwitch();
    Switch isNumber();
    Switch isText();
    Switch isMix();
    Switch isTree();
    template<class T>
    Mix<T> mix();
    template<class T>
    Map<T> map();
    Mix<Tree>& list();
    Map<Tree>& tree();

    // Complex methods
    Tree& get(Text name);
    Tree& get(Number index);
    Tree& path(Mix<Text> path);
    template<class T>
    Tree& set(Text name, T value);
    template<class T>
    Tree& set(Number index, T value);
    template<class T>
    void join(T val);
    void leave();
    Text key();
    Number len();
    Tree& last();
    void remove(Text key);

    // Dumping entities
    void log();
    Text dump();
    Text dumpValue(Tree& val, Number indent = 0, Switch style = off);
    Text dumpTree(Map<Tree>& val, Number indent = 0, Switch style = off);
    Text style(Text val, Text col, Switch style);

    // Parsing entities
    Mix<Text> lex(Text literal);
    Mix<Mix<Mix<Text>>> chunk(Mix<Text> lex);
    Tree parseTree(Number index, Mix<Mix<Mix<Text>>>& chunks);
    Tree parseValue(Mix<Text> val, Mix<Mix<Mix<Text>>>& chunks);
    Switch validate(Text name);
    Text parseText(Text val, Switch slice = off, Switch rawText = off);

    // Methods not meant to be used by a user
    static bool compareAlpha(std::pair<Text,Tree> a, std::pair<Text,Tree> b);

    // Operators
    Tree& operator[](Text name);
    Tree& operator[](Number name);
    Tree& operator[](int name);
    void operator=(Text val);
    void operator=(const char* val);
    void operator=(Switch val);
    void operator=(int val);
    void operator=(Number val);
    void operator=(None val);
    template<class T>
    void operator=(std::initializer_list<T> val);
    template<class T>
    void operator=(Mix<T> val);
    template<class T>
    void operator=(Map<T> val);
    void operator=(Tree val);
};

#ifndef COMPILED
// Create a Tree type for debugging
logType(Tree, {
    std::cout << (std::string)val.dumpValue(val, 0, on).trim() << std::endl;
})
#endif


// --- Header Implementation ---


template <typename T, typename... Args>
Tree::Tree(Text key, T val, Args... args) {
    this->init(key, val, args...);
}

template <typename... Args>
void Tree::init(Text key, Switch val, Args... args)  {
    this->set(key, val);
    this->init(args...);
}

template <typename... Args>
void Tree::init(Text key, Number val, Args... args)  {
    this->set(key, val);
    this->init(args...);
}

template <typename... Args>
void Tree::init(Text key, Text val, Args... args)  {
    this->set(key, val);
    this->init(args...);
}

template <typename... Args>
void Tree::init(const char* key, const char* val, Args... args)  {
    this->set(Text(key), Text(val));
    this->init(args...);
}

template <typename T, typename... Args>
void Tree::init(Text key, Mix<T> val, Args... args)  {
    this->set(key, val);
    this->init(args...);
}

template <typename... Args>
void Tree::init(Text key, Tree val, Args... args)  {
    this->set(key, val);
    this->init(args...);
}

// Tree value conversion
template<class T>
T Tree::cast() {
    try {
        if constexpr (std::is_same<T,Switch>::value) {
            if (!this->isSwitch()) throw Failure(1, Text("Cannot convert '@' to user requested 'switch'", {this->getType()}));
            return (this->value == "on") ? on : off;
        }
        if constexpr (std::is_same<T,Number>::value) {
            if (!this->isNumber()) throw Failure(2, Text("Cannot convert '@' to user requested 'number'", {this->getType()}));
            return Number(std::stod(this->value));
        }
        if constexpr (std::is_same<T,Text>::value) {
            if (!this->isText()) throw Failure(3, Text("Cannot convert '@' to user requested 'text'", {this->getType()}));
            Text res = Text(this->value).slice(1, -1);
            Text result = "";
            Switch escape = off;
            loopWith(res, i, sym) {
                Text next = (i < res.len() - Number(1)) ? res[i+Number(1)] : "";
                // Newline parser
                if (sym == "\\" && next == "n") {
                    result += "\n";
                    escape = on;
                    continue;
                }
                // Tab parser
                if (sym == "\\" && next == "t") {
                    result += "\t";
                    escape = on;
                    continue;
                }
                // Apostrophy parser
                if (sym == "\\" && next == "'") {
                    result += "'";
                    escape = on;
                    continue;
                }
                // Symbol add
                if (!escape)
                    result += sym;
                escape = off;
            }
            return result;
        }
    }
    catch (...) {
        throw Failure(1,
            Text("Tree fromat cast of attribute '@' and type '@' is not convertable.\nPlease use more complex methods to convert like:\nlist() or tree()",
            {this->name, this->getType()})
        );
    }
    return T();
}

// Cast list of it's type to mix
// Works only if it's a mix of primitives
template<class T>
Mix<T> Tree::mix() {
    Mix<T> res;
    loopWith(this->array, i, val) {
        res.join(val.cast<T>());
    }
    return res;
}

// Set value to a tree
template<class T>
Tree& Tree::set(Text name, T value) {
    Tree branch;
    branch = value;
    return this->branches.set(name, branch);
}

// Set value to an array
template<class T>
Tree& Tree::set(Number index, T value) {
    Text id = index;
    if (this->array.len() < index + Number(1) || index < 0) {
        this->error(Text("Index out of range! : @", {id}), {id});
    }
    Tree branch;
    branch = value;
    this->array[index] = branch;
    return this->array[index];
}

// Push value to the list
template<class T>
void Tree::join(T val) {
    Tree branch;
    branch = val;
    this->type = TreeTypes::MIX;
    this->array.join(branch);
}

// Assign a tree (map<T>) to the field
template<class T>
void Tree::operator=(Map<T> val) {
    this->type = TreeTypes::TREE;
    this->value = "";
    this->branches.clear();
    this->array.clear();
    Tree branch;
    loopWith(val, index, key) {
        // Bad field name
        if (!this->validate(val[key])) {
            this->error("Bad field name", {key});
            return;
        }
        branch = val[key];
        this->branches.set(key, branch);
    }
}

// Assign a list (initializer_list) to the field
template<class T>
void Tree::operator=(std::initializer_list<T> val) {
    this->type = TreeTypes::MIX;
    this->value = "";
    this->branches.clear();
    this->array.clear();
    Tree branch;
    loop(val, i) {
        branch = *(val.begin() + i);
        this->array.join(branch);
    }
}

// Assign a list (array) to the field
template<class T>
void Tree::operator=(Mix<T> val) {
    this->type = TreeTypes::MIX;
    this->value = "";
    this->branches.clear();
    this->array.clear();
    Tree branch;
    loopWith (val, index, key) {
        branch = (T)val[index];
        this->array.join(branch);
    }
}


// --- TREE ---


// Set value to a tree
// template<class T>
// Tree& Tree::set(Text name, T value) {
//     Tree branch;
//     branch = value;
//     return this->data.set(name, branch);
// }
