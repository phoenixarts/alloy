Text::Text() {}

// Create a new Text based
// on a C++ string type
Text::Text(const std::string& value) {
    this->value = value;
}

// Create a new Text based
// on a C string type
Text::Text(const char* value) {
    this->value = std::string(value);
}

// Create a new Text based
// on a char type
Text::Text(char value) {
    this->value = std::string(1, value);
}

// Generate a text with a format
Text::Text(const Text& main, std::initializer_list<Text> values) {
    Number offset = 0;
    Switch skip = off;
    this->value.clear();
    auto* ini = values.begin();
    loopWith(main, i, val) {
        if (val == "\\" && !skip) {
            skip = on;
            continue;
        }
        else if (val == "@" && !skip) {
            this->value += (ini + offset.cint())->value;
            offset++;
        }
        else {
            this->value += val;
        }
        if (skip)
            skip = off;
    }
}

// Get length of the text
Number Text::len() const {
    return Number(this->value.length());
}

// Get symbol in a type of Text
// at current index in the Text
Text Text::get(Number index) const {
    return Text(this->value[index]);
}

// Sum two text
Text Text::sum(Text other) {
    return this->value + other.value;
}

// Get a quote from the text
Text Text::slice(Number start, Number end) {
    Text txt = "";
    Number len = this->value.length();
    // If the slice will
    // be length of zero
    if (start == end) return txt;
    // Count from the end
    if (start < 0)
        start = len + start;
    else if (end <= 0)
        end = len + end;
    // If index is inapropriate
    if (start < 0)
        throw Failure(1, Text("Couldn't slice text, starting point is less than zero (asked for: @)", {start}), __func__);
    if (end > len)
        throw Failure(2, Text("Couldn't slice text, ending point is greater than text length (asked for: @)", {end}), __func__);
    // The slice logic
    // log("len: " + Text(len));
    // log("start: " + Text(start));
    // log("end: " + Text(end));
    // log("end-start: " + Text(end-start));
    return this->value.substr(start, (end-start).round());
}

// Overwrite a symbol in the
// Text at given index
Text Text::set(Number index, Text other) {
    if (other.value.length() != 1)
        throw Failure(1, Text("Text to assign should be exactly 1 character long (given: @)", {index}), __func__);
    this->value[index] = other.value[0];
    return *this;
}

// Checks if text has a given symbol
// (note: function requires 1
// symbol long Text value)
Switch Text::has(Text symbol) {
    if (symbol.len() != Number(1)) {
        throw Failure(1, Text("Text of length 1 was requested, given: '@' with size of: @", {symbol, symbol.len()}), __func__);
    }
    for (auto& sym : this->value)
        if (sym == symbol.value[0]) return on;
    return off;
}

// Get index of given value
// (note: returns index of the
// first found character and requires
// Text which is one symbol long)
Number Text::index(Text symbol) {
    if (symbol.len() != Number(1)) {
        throw Failure(1, Text("Text of length 1 was requested, given: '@' with size of: @", {symbol, symbol.len()}), __func__);
    }
    loop ((*this), iter) {
        Text sym = this->value[iter];
        if (sym == symbol) return iter;
    }
    return -1;
}

// Reverses text
Text& Text::reverse() {
    std::reverse(this->value.begin(), this->value.end());
    return *this;
}

// Trims text on both
// left and right side
Text& Text::trim() {
    Switch begin = on;
    Switch end = on;
    Text triml = "";
    Text trimr = "";
    loopWith ((*this), i, sym) {
        if (sym != " " && sym != "\n") {
            triml += sym;
            begin = off;
        }
        if (sym == " " || (sym == "\n" && !begin)) {
            triml += sym;
        }
    }
    triml.reverse();
    loopWith (triml, i, sym) {
        if (sym != " " && sym != "\n") {
            trimr += sym;
            end = off;
        }
        if (sym == " " || (sym == "\n" && !end)) {
            trimr += sym;
        }
    }
    trimr.reverse();
    this->value = trimr.value;
    return (*this);
}

// Uppercase the text
Text& Text::upper(Switch first) {
    if (first) {
        this->value[0] = toupper(this->value[0]);
    }
    else {
        loopFromTo(i, 0, this->len()) {
            this->value[i] = toupper(this->value[i]);
        }
    }
    return (*this);
}

// Lowercase the text
Text& Text::lower(Switch first) {
    if (first) {
        this->value[0] = tolower(this->value[0]);
    }
    else {
        loopFromTo(i, 0, this->len()) {
            this->value[i] = tolower(this->value[i]);
        }
    }
    return (*this);
}

// Checks if given text equals current one
Switch Text::equals(Text other) {
    return this->value == other.value;
}

// Checks if current text
// can be converted to number
Switch Text::isNumber() {
    Text cur = this->value;
    Switch dot = off;
    if (cur == ".") return off;
    if (!this->len()) return off;
    if (Text("+-").has(cur[0]))
        cur = cur.slice(1);
    loopWith (cur, iter, sym) {
        if (sym == Text(".") && !dot) {
            dot = on;
            continue;
        }
        if (sym == "." && dot) return off;
        if (!isdigit(sym.value[0])) return off;
    }
    return on;
}

// Checks if current text consists
// only of alphabetical symbols
Switch Text::isAlpha() {
    Text cur = this->value;
    if (!this->len()) return off;
    loopWith (cur, iter, sym) {
        if (isalpha(sym.value[0])) {
            continue;
        }
        return off;
    }
    return on;
}

// Set string to desired length by
// adding spaces on the left or right
Text& Text::width(Number length, Switch left) {
    if (length == this->len()) return *this;
    else if (length > this->len()) {
        Number delta = length - this->len();
        if (left)
            this->value = std::string(delta, ' ') + this->value;
        else
            this->value += std::string(delta, ' ');
    }
    else if (length < this->len()) {
        this->value = this->slice(0, length);
    }
    return *this;
}

// Transforms text to a C string
// (note: this function is for
// native compatibility only)
const char* Text::cstr() {
    return this->value.c_str();
}


// --- Static methods ---


// Concatnates mix of texts into a one text value
Mix<Text> Text::split(Text separator) {
    Mix<Text> base;
    Text text = "";
    for (int i = 0; i < this->value.length(); i++) {
        if (this->value.length() - i >= separator.value.length()) {
            if (this->slice(i, separator.value.length() + i) == separator) {
                base.join(text);
                text = "";
                i += separator.value.length() - 1;
                continue;
            }
        }
        text += std::string(1, this->value[i]);
    }
    if (text.len()) base.join(text);
    return base;
}

// Concatnates mix of texts into a one text value
Text Text::join(Mix<Text> mix) {
    Text base = "";
    for (int i = 0; i < mix.len(); i++) {
        Text& item = mix[i];
        base += item;
        if (i != mix.len() - Number(1)) {
            base += this->value;
        }
    }
    return base;
}


// --- C++ Operators ---


Text::operator std::string() {
    return this->value;
}

Text Text::operator [](Number index) {
    return this->get(index);
}

Text::operator Switch() {
    if (this->value.length() > 0) return on;
    else return off;
}

Text Text::operator+=(Text other) {
    return this->value = this->sum(other);
}

Switch Text::operator==(Text other) {
    return this->equals(other);
}

Switch Text::operator!=(Text other) {
    return !this->equals(other);
}

Text operator+(Text left, Text right) {
    return left.sum(right);
}

Text operator+(const char* left, Number right) {
    return Text(left) + Text(right);
}

Text operator+(const char* left, Text right) {
    return Text(left).sum(right);
}
