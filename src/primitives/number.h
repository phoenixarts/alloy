
class Number {
public:
    double value = 0;

    Number();
    Number(double value);
    Number(Text value);
    Number(Switch value);

    Number toInt();
    Number round();
    Number floor();
    Number ceil();
    Number trunc();
    Switch isInt();
    int cint() const;

    Number sum(const Number& other);
    Number substract(const Number& other);
    Number multiply(const Number& other);
    Number divide(const Number& other);
    Number modulo(const Number& other);
    Number power(const Number& other);
    Number& increment();
    Number& decrement();
    Switch gt(const Number& other);
    Switch lt(const Number& other);
    Switch gteq(const Number& other);
    Switch lteq(const Number& other);

    operator double();
    operator Switch();
    operator Text();
    Text fixed(const Number& precision);

    Number& operator += (const Number& other);
    Number operator + (const Number& other);
    Number operator - (const Number& other);
    Number operator * (const Number& other);
    Number operator / (const Number& other);
    Number operator % (const Number& other);
    Number operator + (const Switch& other);
    Number operator - (const Switch& other);
    Number operator * (const Switch& other);
    Number operator / (const Switch& other);
    Number operator % (const Switch& other);
    Number& operator ++ (int _);
    Number& operator -- (int _);
    friend Number operator+(const Number& left, int right);
    friend Number operator-(const Number& left, int right);
};
