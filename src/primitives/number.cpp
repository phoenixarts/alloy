Number::Number() {}

// Number constructor with a double type
Number::Number(double value) {
    this->value = value;
}

// Number constructor with a Text type
// (note: this function may fail)
Number::Number(Text value) {
    try {
        this->value = std::stod(value);
    }
    catch (...) {
        throw Failure(1, Text("Coudn't convert '@' to number", {value}), __func__);
    }
}

// Number constructor with a Switch type
Number::Number(Switch value) {
    this->value = value.value;
}

// Sums two numbers and returns the result
Number Number::sum(const Number& other) {
    return this->value + other.value;
}

// Subtracts two numbers and returns the result
Number Number::substract(const Number& other) {
    return this->value - other.value;
}

// Multiplies two numbers and returns the result
Number Number::multiply(const Number& other) {
    return this->value * other.value;
}

// Divides two numbers and returns the result
Number Number::divide(const Number& other) {
    return this->value / other.value;
}

// Divides two numbers and returns the result
Number Number::power(const Number& other) {
    return std::pow(this->value, other.value);
}

// Divides two numbers and returns the result
Number Number::modulo(const Number& other) {
    return (int)this->value % (int)other.value;
}

// Compare Functions

Switch Number::gt(const Number& other) {
    return (this->value > other.value) ? on : off;
}

Switch Number::lt(const Number& other) {
    return (this->value < other.value) ? on : off;
}

Switch Number::gteq(const Number& other) {
    return (this->value >= other.value) ? on : off;
}

Switch Number::lteq(const Number& other) {
    return (this->value <= other.value) ? on : off;
}

// Increment functions

// Increments current number
Number& Number::increment() {
    this->value++;
    return *this;
}

// Decrements current number
Number& Number::decrement() {
    this->value--;
    return *this;
}

// Converts current number to integer
Number Number::toInt() {
    return Number((int)this->value);
}

// Rounds the number
Number Number::round() {
    return Number(::round(this->value));
}

// Floors the number
Number Number::floor() {
    return Number(::floor(this->value));
}

// Ceils the number
Number Number::ceil() {
    return Number(::ceil(this->value));
}

// Truncate the number
Number Number::trunc() {
    return Number(::trunc(this->value));
}

// Checks if current number is an integer
Switch Number::isInt() {
    return Switch((int)this->value == this->value);
}

// Returns a C type integer
// (note: this function is
// for native compatibility only)
int Number::cint() const {
    return (int)this->value;
}

// --- C++ Operators ---


Number::operator double() {
    return this->value;
}

Number::operator Switch() {
    return (this->value == 0) ? off : on;
}

Number::operator Text() {
    // Create Precision
    std::ostringstream out;
    out.precision(12);
    out << std::fixed << this->value;
    // Convert to string
    std::string str = out.str();
    std::reverse(str.begin(), str.end());
    // Remove trailing zeros
    std::string newstr;
    bool zero = true;
    for (char& ch : str) {
        if (ch == '0' && zero) continue;
        if (ch == '.' && zero) {
            zero = false;
            continue;
        }
        if (ch != '0' && zero) zero = false;
        newstr += ch;
    }
    std::reverse(newstr.begin(), newstr.end());
    return Text(newstr);
}

Text Number::fixed(const Number& precision) {
    // Create Precision
    std::ostringstream out;
    out.precision(precision.cint());
    out << std::fixed << this->value;
    // Convert to string
    std::string str = out.str();
    std::reverse(str.begin(), str.end());
    // Remove trailing zeros
    std::string newstr;
    bool zero = true;
    for (char& ch : str) {
        if (ch == '0' && zero) continue;
        if (ch == '.' && zero) {
            zero = false;
            continue;
        }
        if (ch != '0' && zero) zero = false;
        newstr += ch;
    }
    std::reverse(newstr.begin(), newstr.end());
    return Text(newstr);
}

Number& Number::operator+=(const Number& other) {
    this->value = this->sum(other).value;
    return *this;
}

Number Number::operator+(const Number& other) {
    return this->sum(other);
}

Number Number::operator-(const Number& other) {
    return this->substract(other);
}

Number Number::operator*(const Number& other) {
    return this->multiply(other);
}

Number Number::operator/(const Number& other) {
    return this->divide(other);
}

Number Number::operator%(const Number& other) {
    return this->modulo(other);
}

Number Number::operator+(const Switch& other) {
    return this->sum(Number(other));
}

Number Number::operator-(const Switch& other) {
    return this->substract(Number(other));
}

Number Number::operator*(const Switch& other) {
    return this->multiply(Number(other));
}

Number Number::operator/(const Switch& other) {
    return this->divide(Number(other));
}

Number Number::operator%(const Switch& other) {
    return this->modulo(Number(other));
}

Number& Number::operator++(int _) {
    return this->increment();
}

Number& Number::operator--(int _) {
    return this->decrement();
}


// --- Friend operators ---

Number operator+(const Number& left, int right) {
    return left + Number(right);
}

Number operator-(const Number& left, int right) {
    return left - Number(right);
}

// Number Number::operator-(double other) {
//     return this->substract(other);
// }

// Number Number::operator*(double other) {
//     return this->multiply(other);
// }

// Number Number::operator/(Number other) {
//     return this->divide(other);
// }
