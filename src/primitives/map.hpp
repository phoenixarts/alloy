

template<class T>
class Map {
public:
    std::deque<std::pair<Text,T>> value;

    Map();
    Map(std::initializer_list<std::pair<Text,T>> val);

    Number len();
    T& get(const Text& key);
    Text get(Number index);
    T& set(const Text& key, T value);
    T& set(Number index, T value);
    Map<T>& remove(const Text& key);
    Map<T>& clear();
    Switch has(Text key);
    Mix<Text> keys();
    Mix<T> values();

    T& operator[](Text key);
    Text operator[](Number key);
};

// --- Header Implementation --- 


template<class T>
Map<T>::Map() {}


// Initialize map with initializer list
template<class T>
Map<T>::Map(std::initializer_list<std::pair<Text,T>> val) {
    value.clear();
    for (auto item : val) {
        value.push_back(item);
    }
}

// Show length of the map
template<class T>
Number Map<T>::len() {
    return this->value.size();
}

// Get child of the map by key
template<class T>
T& Map<T>::get(const Text& key) {
    for (Number i = 0; i < this->value.size(); i++) {
        auto& item = this->value[i];
        if (item.first == key) {
            return item.second;
        }
    }
    throw Failure(1, Text("Map doesn't contain asked key '@'", {key}), __func__);
}

// Get child of the map by index
template<class T>
Text Map<T>::get(Number index) {
    if (index < 0)
        throw Failure(1,
            Text(
                "Provided index is smaller than zero in the map \n(asked for: @, current size: @)",
                {index, Number(this->value.size())}),
            __func__
        );
    if (index >= this->value.size())
        throw Failure(2,
            Text(
                "Provided index is higher or equal to size of the map \n(asked for: @, current size: @)",
                {index, Number(this->value.size())}),
            __func__
        );
    return this->value[index].first;
}

// Set child of the map by key
template<class T>
T& Map<T>::set(const Text& key, T value) {
    for (Number i = 0; i < this->value.size(); i++) {
        auto& item = this->value[i];
        if (item.first == key) {
            item.second = value;
            return item.second;
        }
    }
    this->value.push_back(std::make_pair(key, value));
    return this->get(key);
}

// Set child of the map by index
template<class T>
T& Map<T>::set(Number index, T value) {
    if (index < 0)
        throw Failure(1,
            Text(
                "Provided index is smaller than zero in the map \n(asked for: @, current size: @)",
                {index, Number(this->value.size())}),
            __func__
        );
    if (index >= this->value.size())
        throw Failure(2,
            Text(
                "Provided index is higher or equal to size of the map \n(asked for: @, current size: @)",
                {index, Number(this->value.size())}),
            __func__
        );
    return this->value[index] = value;
}

// Remove child of the map by key
template<class T>
Map<T>& Map<T>::remove(const Text& key) {
    for (Number i = 0; i < this->value.size(); i++) {
        auto& item = this->value[i];
        if (item.first == key) {
            this->value.erase(this->value.begin() + i);
        }
    }
    return *this;
}

// Clear this map
template<class T>
Map<T>& Map<T>::clear() {
    this->value.clear();
    return *this;
}

// Check if map has given key
template<class T>
Switch Map<T>::has(Text key) {
    for (Number i = 0; i < this->value.size(); i++) {
        if (this->value[i].first == key) return on;
    }
    return off;
}

// Return all the keys
template<class T>
Mix<Text> Map<T>::keys() {
    Mix<Text> res;
    for (Number i = 0; i < this->value.size(); i++) {
        res.join(this->value[i].first);
    }
    return res;
}

// Return all the values
template<class T>
Mix<T> Map<T>::values() {
    Mix<T> res;
    for (Number i = 0; i < this->value.size(); i++) {
        res.join(this->value[i].second);
    }
    return res;
}


// --- C++ Operators ---


template<class T>
T& Map<T>::operator [](Text key) {
    return this->get(key);
}

template<class T>
Text Map<T>::operator [](Number index) {
    return this->get(index);
}