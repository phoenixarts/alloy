Switch::Switch() {}

// Create a Switch type 
// based on a boolean type
Switch::Switch(bool value) {
    this->value = value;
}


// --- C++ Operators ---


Switch Switch::operator!() {
    return Switch(!this->value);
}

Number Switch::operator+(Number other) {
    return other.sum(this->value);
}

Number Switch::operator-(Number other) {
    return other.substract(this->value);
}

Number Switch::operator*(Number other) {
    return other.multiply(this->value);
}

Number Switch::operator/(Number other) {
    return other.divide(this->value);
}

Number Switch::operator%(Number other) {
    return other.modulo(this->value);
}

Number Switch::operator+(Switch other) {
    return Number(other).sum(this->value);
}

Number Switch::operator-(Switch other) {
    return Number(other).substract(this->value);
}

Number Switch::operator*(Switch other) {
    return Number(other).multiply(this->value);
}

Number Switch::operator/(Switch other) {
    return Number(other).divide(this->value);
}

Number Switch::operator%(Switch other) {
    return Number(other).modulo(this->value);
}

Switch::operator bool() {
    return this->value;
}

Switch::operator Number() {
    return Number((double)this->value);

}
Switch::operator Text() {
    if (this->value) return Text("on");
    else return Text("off");
}