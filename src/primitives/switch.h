
class Switch {
public:
    bool value = false;

    Switch();
    Switch(bool value);

    Switch operator ! ();
    Number operator + (Number other);
    Number operator - (Number other);
    Number operator * (Number other);
    Number operator / (Number other);
    Number operator % (Number other);
    Number operator + (Switch other);
    Number operator - (Switch other);
    Number operator * (Switch other);
    Number operator / (Switch other);
    Number operator % (Switch other);

    operator bool();
    operator Number();
    operator Text();
};

#define on Switch(true)
#define off Switch(false)