
template<class T>
class Mix {
public:
    std::deque<T> value;

    Mix();
    Mix(std::deque<T> other);
    Mix(std::vector<T> other);
    Mix(std::initializer_list<T> other);

    Number len();
    T& get(Number index);
    void set(Number index, T value);
    Mix<T>& fill(T val, Number count);
    Mix<T>& join(T val);
    Mix<T>& leave();
    Mix<T>& push(T val);
    Mix<T>& pull();
    Mix<T>& reverse();
    Mix<T>& clear();
    Mix<T> sum(Mix<T> value);
    T& last();
    Switch has(T val);
    Number index(T val);
    Mix<T> slice(Number start, Number end = -0.1);

    T& operator[](Number index);
    Mix<T> operator+(Mix<T> value);
    Mix<T> operator+=(Mix<T> value);
};


// --- Header Implementation ---


template<class T>
Mix<T>::Mix() {}

// Create a mix with deque type
template<class T>
Mix<T>::Mix(std::deque<T> other) {
    this->value = other;
}

// Create a mix with vector type
template<class T>
Mix<T>::Mix(std::vector<T> other) {
    this->value.clear();
    for (auto& item : other) {
        this->value.push_back(item);
    }
}

// Create a mix with
// initializer list
template<class T>
Mix<T>::Mix(std::initializer_list<T> other) {
    this->value = std::deque<T>(other);
}

// Join a new element to
// the end of the mix
template<class T>
Mix<T>& Mix<T>::join(T val) {
    this->value.push_back(val);
    return *this;
}

// Leave the last
// element from the mix
template<class T>
Mix<T>& Mix<T>::leave() {
    if (this->value.size())
        this->value.pop_back();
    return *this;
}

// Push a new element to
// the beginning of the mix
template<class T>
Mix<T>& Mix<T>::push(T val) {
    this->value.push_front(val);
    return *this;
}

// Pull out the first
// element from the mix
template<class T>
Mix<T>& Mix<T>::pull() {
    if (this->value.size())
        this->value.pop_front();
    return *this;
}

// Reverse the mix
template<class T>
Mix<T>& Mix<T>::reverse() {
    std::reverse(this->value.begin(), this->value.end());
    return *this;
}

// Get length of the mix
template<class T>
Number Mix<T>::len() {
    return this->value.size();
}

// Get child by index
template<class T>
T& Mix<T>::get(Number index) {
    if (index < 0)
        throw Failure(1,
            Text(
                "Provided index is smaller than zero in the mix \n(asked for: @, current size: @)",
                {index, Number(this->value.size())}
            ),
            __func__
        );
    if (index >= this->value.size())
        throw Failure(2,
            Text(
                "Provided index is higher or equal to size of the mix \n(asked for: @, current size: @)",
                {index, Number(this->value.size())}
            ),
            __func__
        );
    return this->value[index.cint()];
}

// Set child by index
template<class T>
void Mix<T>::set(Number index, T value) {
    if (index < 0)
        throw Failure(1,
            Text(
                "Provided index is smaller than zero in the mix \n(asked for: @, current size: @)",
                {index, Number(this->value.size())}
            ),
            __func__
        );
    if (index >= this->value.size())
        throw Failure(2,
            Text(
                "Provided index is higher or equal to size of the mix \n(asked for: @, current size: @)",
                {index, Number(this->value.size())}
            ),
            __func__
        );
    this->value[index] = value;
}

// Determines if mix
// contains given value
template<class T>
Mix<T>& Mix<T>::fill(T val, Number count) {
    this->value.clear();
    loopFromTo(i, 0, count) {
        this->join(val);
    }
    return *this;
}

// Determines if mix
// contains given value
template<class T>
Number Mix<T>::index(T val) {
    loopFromTo(i, 0, this->value.size()) {
        if (this->value[i] == val) return i;
    }
    return -1;
}

// Determines if mix
// contains given value
template<class T>
Switch Mix<T>::has(T val) {
    for (T& item : this->value)
            if (item == val) return on;
    return off;
}

// Remove all elements
// from the mix
template<class T>
Mix<T>& Mix<T>::clear() {
    this->value.clear();
    return *this;
}

// Merge other mix
// into this one
// by extending it
// on the end side
template<class T>
Mix<T> Mix<T>::sum(Mix<T> value) {
    Mix<T> res = this->value;
    loopWith(value, index, val) {
        res.join(val);
    }
    return res;
}

// Get the last
// element in the mix
template<class T>
T& Mix<T>::last() {
    return this->get(this->len()--);
}

// Slice the mix in order to get a new
// mix which is a submix of the mix
template<class T>
Mix<T> Mix<T>::slice(Number start, Number end) {
    Mix<T> mix;
    // If the slice will
    // be length of zero
    if (start == end) return mix;
    // Count from the end
    if (start < 0)
        start = this->value.size() + start;
    if (end <= 0)
        end = this->value.size() + end;
    // If index is inapropriate
    if (start < 0)
        throw Failure(1, Text("Couldn't slice mix, starting point is less than zero (asked for: @)", {start}), __func__);
    if (end > this->value.size())
        throw Failure(2, Text("Couldn't slice mix, ending point is greater than mix length (asked for: @)", {end}), __func__);
    // The slice logic
    for (int i = start; i < end; i++)
        mix.join(this->value[i]);
    return mix;
}


// --- C++ Operators ---


template<class T>
T& Mix<T>::operator[](Number index) {
    return this->get(index);
}

template<class T>
Mix<T> Mix<T>::operator+(Mix<T> value) {
    return this->sum(value);
}

template<class T>
Mix<T> Mix<T>::operator+=(Mix<T> value) {
    return this->value = this->sum(value).value;
}
