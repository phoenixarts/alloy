
class Text {
public:
    std::string value;

    Text();
    Text(const std::string& value);
    Text(const char* value);
    Text(char value);
    Text(const Text& main, std::initializer_list<Text> values);

    Text sum(Text other);
    Text get(Number index) const;
    Text set(Number index, Text value);

    Switch equals(Text value);
    Number len() const;
    Text slice(Number start, Number end = -0.1);
    Switch has(Text symbol);
    Number index(Text symbol);
    Text& reverse();
    Text& trim();
    Text& upper(Switch first = off);
    Text& lower(Switch first = off);
    Switch isNumber();
    Switch isAlpha();
    Text join(Mix<Text> mix);
    Mix<Text> split(Text separator);
    Text& width(Number length, Switch left = off);
    const char* cstr();
    

    operator std::string();
    operator Switch();
    
    Text operator [](Number index);
    Text operator+=(Text other);
    Switch operator==(Text other);
    Switch operator!=(Text other);
    friend Text operator+(Text left, Text right);
    friend Text operator+(const char* left, Number right);
    friend Text operator+(const char* left, Text right);

};