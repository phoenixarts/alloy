#ifdef _WIN32

class Module {
public:
    HMODULE handle = nullptr;
    Switch loaded = off;
    Text path = "";

    Module();
    Module(Text path);
    void open();
    template<class T>
    T load(Text name);
    void close();
};

// Laod a function
// from the module
template<class T>
T Module::load(Text name) {
    if (!this->loaded) throw Failure(1, Text("Couldn't load closed module\n(@)", {this->path}));
    T function = nullptr;
    char rawname[512];
    strcpy_s(rawname, name.cstr());
    void* dllfun = GetProcAddress(this->handle, rawname);
    reinterpret_cast<void*&>(function) = dllfun;
    char* error;
    if (dllfun == NULL)  {
        throw Failure(2,
            Text(
                "Loading function '@' from Magma Rumtime Module failed.\n",
                {name}
            ),
            __func__
        );
    }
    return function;
}

#else

class Module {
public:
    void* handle = nullptr;
    Switch loaded = off;
    Text path = "";

    Module();
    Module(Text path);
    void open();
    template<class T>
    T load(Text name);
    void close();
};

// Laod a function
// from the module
template<class T>
T Module::load(Text name) {
    if (!this->loaded) throw Failure(1, Text("Couldn't load closed module\n(@)", {this->path}));
    T function = nullptr;
    char rawname[512];
    std::strcpy(rawname, name.cstr());
    void* dllfun = dlsym(this->handle, rawname);
    reinterpret_cast<void*&>(function) = dllfun;
    char* error;
    if ((error = dlerror()) != NULL)  {
        throw Failure(2,
            Text(
                "Loading function '@' from Magma Rumtime Module failed.\nMore details:\n\n@",
                {name, dlerror()}
            ),
            __func__
        );
    }
    return function;
}

#endif
