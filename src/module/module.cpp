Module::Module() {}

#ifdef _WIN32
// --- Windows ---
// This part of code is built for
// both windows support

// Load a module to memory
Module::Module(Text path) {
    this->path = path;
    this->open();
}

// Open the module
void Module::open() {
    if (this->loaded) return;
    char rawpath[512];
    strcpy_s(rawpath, this->path.cstr());
    this->handle = LoadLibrary(rawpath);
    if (this->handle == NULL) {
        throw Failure(1,
            Text(
                "Couldn't load Magma Runtime Module.\nPath: '@'",
                {this->path}
            ),
            __func__
        );
    }
    this->loaded = on;
}

// Close the module
void Module::close() {
    if (!this->loaded) return;
    if (this->handle != nullptr) {
        FreeLibrary((HINSTANCE)this->handle);
    }
    this->loaded = off;
}


#else
// --- Linux / MacOS ---
// This part of code is built for
// both linux and macos support

// Load a module to memory
Module::Module(Text path) {
    this->path = path;
    this->open();
}

// Open the module
void Module::open() {
    if (this->loaded) return;
    char rawpath[512];
    std::strcpy(rawpath, this->path.cstr());
    this->handle = dlopen (rawpath, RTLD_LAZY);
    if (!this->handle) {
        throw Failure(1,
            Text(
                "Couldn't load Magma Runtime Module.\nPath: '@'\nMore details:\n\n@",
                {this->path, dlerror()}
            ),
            __func__
        );
    }
    this->loaded = on;
}

// Close the module
void Module::close() {
    if (!this->loaded) return;
    if (this->handle != nullptr) {
        dlclose(this->handle);
    }
    this->loaded = off;
}

#endif
