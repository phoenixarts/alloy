//         --- LOG ---
// Here is a set of functions
// Which do best to log out values
// It's used mostly for debug purposes

// template<class T>
// void log(T val, Switch newline = on, Number indent = 0);

void log(const char* value, Switch newline = on, Number indent = 0);

void log(Text val, Switch newline = on, Number indent = 0);

void log(Switch val, Switch newline = on, Number indent = 0);

void log(Number val, Switch newline = on, Number indent = 0);

template<class T>
void log(Mix<T> arr, bool newline = on, Number indent = 0);

template<class T>
void log(Map<T> map, Switch newline = on, Number indent = 0);

// // For basic C++ types
// template<class T>
// void log(T val, Switch newline, Number indent) {
//     std::cout << val;
//     if (newline) {
//         std::cout << std::endl;
//     }
// }

// For Mix type
template<class T>
void log(Mix<T> arr, bool newline, Number indent) {
    Text col = "rgb(100, 100, 100)";
    std::cout << color(col, "[");
    loop(arr, i) {
        T& item = arr[i];
        log(item, off);
        if (i != arr.len() - Number(1)) {
            std::cout << color(col, ", ");
        }
    }
    std::cout << color(col, "]");
    if (newline) std::cout << std::endl;
}

// For Map type
template<class T>
void log(Map<T> map, Switch newline, Number indent) {
    Switch contains = map.len();
    Text col = "rgb(100, 100, 100)";
    std::cout << color(col, "<Map> ");
    std::cout << color(col, (contains) ? "{\n" : "{ ");
    loopWith (map, iter, key) {
        auto& val = map[key];
        for (Number i = 0; i < indent; i++) std::cout << "  ";
        std::cout << "  ";
        std::cout << (std::string)key;
        std::cout << " = ";
        log(val, off, indent + Number(1));
        std::cout << "\n";
    }
    if (contains)
        for (Number i = 0; i < indent; i++) std::cout << "  ";
    std::cout << color(col, "}");
    if(newline) std::cout << std::endl;
}

// If it's a header
// #ifndef COMPILED
//
//
//     // Factory macro for any other type
//     #define logType(type, attrs) \
//         void log(type val, Switch newline = on, Number indent = 0);
// #else
    // Factory macro for any other type
    #define logType(type, attrs) \
        void log(type val, Switch newline = on, Number indent = 0) { \
            Text col = "rgb(100, 100, 100)"; \
            std::cout << color(col, Text("<").sum(Text(#type)).sum("> ")); \
            std::cout << color(col, "{\n"); \
            auto logAttr = [&] (Text name, auto attr) { \
                for (int i = 0; i < indent; i++) std::cout << "  "; \
                std::cout << "  "; \
                std::cout << (std::string)name; \
                std::cout << " = "; \
                log(attr, false, indent.sum(Number(1))); \
                std::cout << "\n"; \
            }; \
            attrs \
            for (Number i = 0; i < indent; i++) std::cout << "  "; \
            std::cout << color(col, "}"); \
            if(newline) std::cout << std::endl; \
        }
// #endif
