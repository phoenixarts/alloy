#ifdef _WIN32

// ... PUT HERE SOME WINDOWS CODE

// Create a pipe with desired buffer
Pipe::Pipe(Text name, Number buffer) {
    this->name = name;
    this->path = Text("\\\\.\\pipe\\") + name;
    this->buffer = buffer;
}

// Read data from pipe
Text Pipe::read() {
  HANDLE pipe;
  while(1) {
      pipe = CreateFile(
        this->path.cstr(),
        GENERIC_READ,
        0,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        NULL
      );
      if (pipe != INVALID_HANDLE_VALUE) {
        break;
        throw Failure(1, "For some reason could not connect to the pipe", __func__);
      }
      CloseHandle(pipe);
  }
    char buffer[this->buffer.cint()];
    DWORD numBytesRead = 0;
    BOOL status;
    Text result;
    while ((status = ReadFile(
        pipe,
        buffer,
        this->buffer.cint() * sizeof(char),
        &numBytesRead,
        NULL
    )) > 0) {
        result += Text(buffer);
    }
    CloseHandle(pipe);
    return result;
}

// Write data to pipe
void Pipe::write(Text data) {
    HANDLE pipe = CreateNamedPipe(
        this->path.cstr(),
        PIPE_ACCESS_OUTBOUND,
        PIPE_TYPE_BYTE,
        1,
        0,
        0,
        0,
        NULL
    );
    BOOL status = ConnectNamedPipe(pipe, NULL);
    char rawdata[data.len().cint()+1];
    strncpy(rawdata, data.cstr(), data.len().cint());
    rawdata[data.len().cint()] = '\0';
    DWORD numBytesWritten = 0;
    status = WriteFile(
        pipe,
        rawdata,
        (data.len().cint()+1) * sizeof(char),
        &numBytesWritten,
        NULL
    );
    CloseHandle(pipe);
    if (!status)
        throw Failure(1, Text("Failed to send data through pipe '@'", {this->name}), __func__);
}

// Remove pipe file
void Pipe::remove() {}

#else

// Create a pipe with desired buffer
Pipe::Pipe(Text name, Number buffer) {
    this->buffer = buffer;
    this->name = name;
    this->path = Text("/tmp/@.pipe", {name});
    if (mkfifo(this->path.cstr(), 0777) == -1) {
        if (errno != EEXIST) {
            throw Failure(1, "For some reason could not create named pipe file", __func__);
        }
    }
}

// Read data from pipe
Text Pipe::read() {
    int fd = open(this->path.cstr(), O_RDONLY);
    Text result;
    char buffer[this->buffer.cint()];
    int status;
    while ( (status = ::read(fd, &buffer, sizeof(buffer)) ) > 0) {
        loopFromTo(i, 0, this->buffer) {
            result += buffer[i.cint()];
        }
    }
    ::close(fd);
    return result;
}

// Write data to pipe
void Pipe::write(Text data) {
    int fd = open(this->path.cstr(), O_WRONLY);
    char rawdata[data.len().cint()+1];
    strncpy(rawdata, data.cstr(), data.len().cint());
    rawdata[data.len().cint()] = '\0';
    ::write(fd, &rawdata, sizeof(rawdata));
    ::close(fd);
}

// Remove pipe file
void Pipe::remove() {
    FS::remove(this->path);
}

#endif
