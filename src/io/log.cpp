//         --- LOG ---
// Here is a set of functions
// Which do best to log out values
// It's used mostly for debug purposes

// For hardcoded text literals (markdown enabled)
void log(const char* value, Switch newline, Number indent) {
    Text col = (newline) ? "natural" : "green";
    Text val = Text(value);
    // Horizontal line
    if (val == "---") {
        val = color("white", "──────────");
        loopFromTo(i, 15, 1) {
            Text rgb = Text("rgb(@,@,@)", {
                i.power(2), i.power(2), i.power(2)
            });
            val += color((std::string)rgb, "─");    
        }
    }
    // Heading style markdown
    if (val[0] == '#' && newline) {
        Switch header = true;
        col = "b:rgb(0,0,0,255,100,0)";
        val = val.slice(1);
        if (val[0] == '#') {
            header = false;
            col = "b:rgb(255,100,0)";
            val = val.slice(1);
            if (val[0] == '#') {
                col = "rgb(255,100,0)";
                val = val.slice(1);
            }
        }
        if (header) {
            val = Text(" ") + val + " ";
        }
    }
    std::cout << color(col, val);
    if (newline) {
        std::cout << std::endl;
    }
}

// For Text type
void log(Text val, Switch newline, Number indent) {
    Text col = (newline) ? "natural" : "green";
    if (!newline) val = "'" + val + "'";
    std::cout << color(col, val);
    if (newline) {
        std::cout << std::endl;
    }
}

// For Switch type
void log(Switch val, Switch newline, Number indent) {
    std::cout << color("rgb(255, 150, 0)", (val) ? "on" : "off");
    if (newline) {
        std::cout << std::endl;
    }
}

// For Number type
void log(Number val, Switch newline, Number indent) {
    std::cout << color("rgb(255, 150, 0)", Text(val));
    if (newline) {
        std::cout << std::endl;
    }
}