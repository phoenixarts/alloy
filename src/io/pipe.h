class Pipe {
public:
    Text name;
    Text path;
    Number buffer;

    Pipe(Text name, Number buffer = 1);
    Text read();
    void write(Text data);
    void remove();
};
