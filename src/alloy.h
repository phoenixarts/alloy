#ifndef COMPILED
    #include "predef.cpp"
#endif
namespace Alloy {

    #include "classes.h"
    #include "misc/colorize.h"

    /* Headers */

    #include "primitives/switch.h"
    #include "primitives/number.h"
    #include "primitives/text.h"
    #include "io/get.h"
    #include "io/put.h"
    #include "io/pipe.h"
    #include "misc/fail.h"
    #include "misc/process.h"
    #include "misc/regexp.h"
    #include "fs/path.h"
    #include "fs/fs.h"
    #include "fs/file.h"

    /* Template classes and implementations */

    #include "misc/loop.hpp"
    #include "io/log.hpp"
    #include "primitives/mix.hpp"
    #include "primitives/map.hpp"
    #include "module/module.hpp"
    #include "tree/tree.hpp"
    #include "gui/window.hpp"
}