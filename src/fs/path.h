class Path {
public:
    
    static Text exec();
    static Text execDir();
    static Text file(Text path);
    static Text dir(Text path);
    static Text ext(Text path);
    static Text absolute(Text path);
    static Text relative(Text path);
    static Switch exists(Text path);
    static Text getType(Text path);
    static Text from(Text path);
    static Text join(Mix<Text> path);
    static Text cwd();
    static Mix<Text> listDir(Text path);

};
