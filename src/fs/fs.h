class FS {
public:

    static void remove(Text path);
    static void directory(Text path);
    static void copy(Text from, Text to);
    static void move(Text from, Text to);

};