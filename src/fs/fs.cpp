// Remove file or directory
void FS::remove(Text path) {
    if (!Path::exists(path)) 
        throw Failure(1, Text("Path '@' doesn't exist", {path}), __func__);
    auto nativepath = std::filesystem::path((std::string)path);
    if (Path::getType(path) == "directory") {
        try {
            std::filesystem::remove_all(nativepath);
        }
        catch (...) {
            throw Failure(1, Text("Couldn't remove directory at \n(@)", {path}));
        }
    }
    else {
        try {
            std::filesystem::remove(nativepath);
        }
        catch (...) {
            throw Failure(1, Text("Couldn't remove file at \n(@)", {path}));
        }
    }
}

// Creates a new directory
void FS::directory(Text path) {
    try {
        std::filesystem::create_directories((std::string)path);
    }
    catch (...) {
        throw Failure(1, Text("Couldn't create directory at \n(@)", {path}));
    }
}

// Copies file or directory (recursively) 
// to another path (note: target path cannot exist)
void FS::copy(Text from, Text to) {
    if (!Path::exists(from)) 
        throw Failure(1, Text("Path '@' doesn't exist", {from}), __func__);
    if (Path::exists(from) && Path::exists(to)) 
        throw Failure(2, Text("Target path '@' already exists", {to}), __func__);
    if (Path::getType(from) == "file" && to.slice(-1) == "/")
        throw Failure(3, Text("Target path '@' is a directory when source is a file", {to}), __func__);
    try {
        std::filesystem::copy((std::string)from, (std::string)to, std::filesystem::copy_options::recursive);
    }
    catch (...) {
        throw Failure(1, Text("Couldn't copy from \n(@)to: \n(@)", {from, to}));
    }
}

// Moves file or directory (recursively) 
// to another path (note: target path cannot exist)
void FS::move(Text from, Text to) {
    if (!Path::exists(from)) 
        throw Failure(1, Text("Path '@' doesn't exist", {from}), __func__);
    if (Path::exists(from) && Path::exists(to)) 
        throw Failure(2, Text("Target path '@' already exists", {to}), __func__);
    if (Path::getType(from) == "file" && to.slice(-1) == "/")
        throw Failure(3, Text("Target path '@' is a directory when source is a file", {to}), __func__);
    try {
        std::filesystem::rename((std::string)from, (std::string)to);
    }
    catch (...) {
        throw Failure(1, Text("Couldn't move from \n(@)to: \n(@)", {from, to}));
    }
}
