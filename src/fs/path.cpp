
// Returns the path to current executable
// Needs a Process to be initialized
Text Path::exec() {
    Text sourcePath = "(error)";
    #ifdef _WIN32
        char pBuf[512];
        size_t len = sizeof(pBuf);
        GetModuleFileName(NULL, pBuf, len);
        sourcePath = Text(pBuf);
    #else
        char pBuf[512];
        size_t len = sizeof(pBuf);
        int bytes = fmin(readlink("/proc/self/exe", pBuf, len), len - 1);
        if(bytes >= 0)
            pBuf[bytes] = '\0';
        sourcePath = Text(pBuf);
    #endif
    auto raw = std::filesystem::path((std::string)sourcePath);
    std::string path = std::filesystem::absolute(raw).lexically_normal().make_preferred().string();
    return Text(path);
}

// Returns a directory where
// current executable is
Text Path::execDir() {
    auto raw = std::filesystem::path((std::string)Path::exec());
    std::string path = std::filesystem::absolute(raw).parent_path().lexically_normal().make_preferred().string();
    return Text(path);
}

// Returns a file name from path
Text Path::file(Text path) {
    if (path.len() == 0) throw Failure(1, "File path must be longer than one character", __func__);
    auto raw = std::filesystem::path((std::string)path);
    std::string baked = raw.filename().lexically_normal().string();
    return Text(baked);
}

// Returns a parent directory
// of current path
Text Path::dir(Text path) {
    if (path.len() == 0) throw Failure(1, "File path must be longer than one character", __func__);
    auto raw = std::filesystem::path((std::string)path);
    std::string baked = std::filesystem::absolute(raw).parent_path().lexically_normal().string();
    return Text(baked);
}

// Returns extension of file
// with path is pointing to
Text Path::ext(Text path) {
    if (path.len() == 0) throw Failure(1, "File path must be longer than one character", __func__);
    auto raw = std::filesystem::path((std::string)path);
    std::string baked = raw.extension().lexically_normal().string();
    return Text(baked);
}

// Returns an absolute
// version of given path
Text Path::absolute(Text path) {
    if (path.len() == 0) throw Failure(1, "File path must be longer than one character", __func__);
    auto raw = std::filesystem::path((std::string)path);
    std::string baked = std::filesystem::absolute(raw).lexically_normal().string();
    return Text(baked);
}

// Returns an relative
// version of given path
Text Path::relative(Text path) {
    if (path.len() == 0) throw Failure(1, "File path must be longer than one character", __func__);
    if (!path.has("/")) return path;
    auto raw = std::filesystem::path((std::string)path);
    std::string baked = std::filesystem::relative(raw).lexically_normal().string();
    return Text(baked);
}

// Checks whether
// given path exists
Switch Path::exists(Text path) {
    if (path.len() == 0) throw Failure(1, "File path must be longer than one character", __func__);
    auto raw = std::filesystem::path((std::string)path);
    return Switch(std::filesystem::exists(raw));
}

// Creates a universal path
// from the supplied text
Text Path::from(Text path) {
    if (path.len() == 0) throw Failure(1, "File path must be longer than one character", __func__);
    auto raw = std::filesystem::path((std::string)path);
    std::string baked = std::filesystem::absolute(raw).lexically_normal().make_preferred().string();
    return Text(baked);
}

// Joins multiple paths
// to one absolute path
Text Path::join(Mix<Text> paths) {
    std::filesystem::path p;
    loop (paths, i) {
        if (paths[i].len() > 1 && i != 0)
        if (paths[i][0] == "/") {
            paths[i] = paths[i].slice(1);
        }
        p /= std::filesystem::path((std::string)paths[i]);
    }
    std::string baked = std::filesystem::absolute(p).lexically_normal().make_preferred().string();
    return Text(baked);
}

// Returns current
// working directory
Text Path::cwd() {
    return Text(std::filesystem::current_path().lexically_normal().make_preferred().string());
}

// Lists directories
// in the current path
Mix<Text> Path::listDir(Text path) {
    if (path.len() == 0) throw Failure(1, "File path must be longer than one character", __func__);
    Mix<Text> res;
    for(auto& p: std::filesystem::directory_iterator((std::string)path))
        res.join((std::string)p.path().string());
    return res;
}

// Returns a type of the entity
// the path is pointing to
Text Path::getType(Text path) {
    if (path.len() == 0) throw Failure(1, "File path must be longer than one character", __func__);
    auto raw = std::filesystem::path((std::string)path);
    auto status = std::filesystem::directory_entry(raw);
    if (status.is_regular_file()) return "file";
    if (status.is_directory()) return "directory";
    if (status.is_symlink()) return "symlink";
    if (status.is_socket()) return "socket";
    if (status.is_fifo()) return "fifo";
    if (status.is_block_file()) return "block";
    if (status.is_character_file()) return "character";
    if (status.is_other()) return "other";
    return "---";
}
