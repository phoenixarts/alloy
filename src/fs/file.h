class File {
public:
    Text path;

    File();
    File(Text path);

    void write(Text data);
    void append(Text data);
    Text read();
    Mix<Text> readLines();
    Text getPath();
    void setPath(Text path);
    void remove();

};
