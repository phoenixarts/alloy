File::File() {}

// Load file from path
File::File(Text path) {
    if (!Path::exists(Path::dir(path)))
        throw Failure(1, Text("Path '@' doesn't exist", {Path::dir(path)}), __func__);
    this->path = path;
    std::ofstream file((std::string)this->path, std::ios_base::app);
    file << "";
}


// Write some data to file
void File::write(Text data) {
    if (!Path::exists(this->path))
        throw Failure(1, Text("File '@' no longer exist", {this->path}), __func__);
    std::ofstream file((std::string)this->path);
    file << (std::string)data;
}

// Append some data to file
void File::append(Text data) {
    if (!Path::exists(this->path))
        throw Failure(1, Text("File '@' no longer exist", {this->path}), __func__);
    std::ofstream file((std::string)this->path, std::ios_base::app);
    file << (std::string)data;
}

// Read some data from file
Text File::read() {
    if (!Path::exists(this->path))
        throw Failure(1, Text("File '@' no longer exist", {this->path}), __func__);
    Text result;
    std::string line;
    std::ifstream file ((std::string)this->path);
    if (file.is_open()) {
        while (getline(file,line)) {
            result += line + "\n";
        }
        file.close();
    }
    else throw Failure(1, Text("Couldn't open file '@'", {path}), __func__);
    return result.slice(0, -1);
}

// Read data from file in Lines
Mix<Text> File::readLines() {
    if (!Path::exists(this->path))
        throw Failure(1, Text("File '@' no longer exist", {this->path}), __func__);
    Mix<Text> result;
    std::string line;
    std::ifstream file ((std::string)this->path);
    if (file.is_open()) {
        while (getline(file,line)) {
            result.join(line);
        }
        file.close();
    }
    else throw Failure(1, Text("Couldn't open file '@'", {path}), __func__);
    return result;
}

// Get path to the file
Text File::getPath() {
    return Path::absolute(this->path);
}

// Set path to the file
void File::setPath(Text path) {
    if (!Path::exists(Path::dir(path)))
        throw Failure(1, Text("Path '@' doesn't exist", {Path::dir(path)}), __func__);
    this->path = path;
    std::ofstream file((std::string)this->path, std::ios_base::app);
    file << "";
}

// Removes file
void File::remove() {
    if (!Path::exists(this->path))
        throw Failure(1, Text("Path '@' doesn't exist", {this->path}), __func__);
    auto nativepath = std::filesystem::path((std::string)this->path);
    std::filesystem::remove(nativepath);
}
