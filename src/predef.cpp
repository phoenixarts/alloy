// Universal C++ Libraries
#include <iostream>
#include <sstream>
#include <algorithm>
#include <functional>
#include <vector>
#include <map>
#include <memory>
#include <exception>
#include <type_traits>
#include <initializer_list>
#include <cstring>
#include <filesystem>
#include <fstream>
#include <cmath>
#include <regex>
#include <variant>
// Universal C Libraries
#include <stdio.h>
#include <stdlib.h>
// --- Global Variables ---
// Tells whether current
// system supports rgb colors
static bool TERM_RGB_COL = true;

// Cross platform layer
#ifdef _WIN32
    #include <windows.h>
    #include "windows.hpp"
#else
    #include <deque>
    #include <dlfcn.h>
    #include <unistd.h>
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <fcntl.h> 
    #include <math.h>
#endif
