#undef None
class None {
public:
    None(){}
};
#define none None()

class Number;
class Switch;
class Text;

template<class T>
class Mix;

template<class T>
class Map;

class Failure;

namespace Old {
    class Tree;
}
namespace New {
    class Tree;
}

class Module;