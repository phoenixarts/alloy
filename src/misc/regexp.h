class RegExp {
public:
    std::regex regexp;

    RegExp() {}
    RegExp(Text reg);
    Switch match(Text line);
    Switch search(Text line);
    Mix<Mix<Text>> groups(Text line);
};
