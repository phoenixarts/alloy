Mix<Text> Process::args = {};

#ifdef __linux__
Text Process::os = "linux";
#elif _WIN32
Text Process::os = "windows";
#elif __APPLE__
Text Process::os = "darwin";
#elif __FreeBSD__
Text Process::os = "freebsd";
#elif __ANDROID__
Text Process::os = "android";
#else
Text Process::os = "(unknown)";
#endif

// Register a new process
void Process::init(int argc, char** argv) {
    for (int i = 0; i < argc; ++i) {
        Process::args.join(Text(argv[i]));
    }

    #ifdef _WIN32
    windowsInit();
    #endif
}
