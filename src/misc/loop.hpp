// Iterate Array with index
#define loop(array, iter) \
    for (Number iter; iter < array.len(); iter++)

// Iterate Array with index and value
#define loopWith(array, iter, val) \
    if (array.len()) \
        for (auto [iter, val] = std::tuple{Number(0), array.get(Number(0))}; iter < array.len(); iter++, val = (iter < array.len()) ? array.get(iter) : array.get(Number(0)))

// Loop From To
#define loopFromTo(iter, from, to) \
    if (from != to) \
        for (Number iter = from; (from < to) ? (iter < to) : (iter >= to) ; (from < to) ? iter++ : iter--)