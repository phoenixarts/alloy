// Color Terminal Output Library
// ----
// blue
// green
// cyan
// red
// magenta
// yellow
// white
// gray
// rgb(255,100,0)
// b:red
// d:green
// ----
// Usage:
// color("red", "Error occured", true);
//       color  text           newLine


#include <iostream>
#include <vector>

// Generates std::string with desired color
std::string color(std::string color, std::string line) {
    std::string col = "\033[0m";
    std::string tmp = "";
    std::string style = "0";
    std::string background = "";
    std::string bg = "";

    // Fallback to basic color range
    if (!TERM_RGB_COL) {
        // Windows CMD and old PowerShell support
        #ifdef _WIN32
        HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
        if (color.length() > 2)
        if (color[1] == ':') color = color.substr(2);
        int found = color.find(':');
        if (found != std::string::npos) {
            background = color.substr(found + 1);
            color = color.substr(0, found);
        }
        int col = 0;
        int bg = 0;
        if (color == "black") col = 0;
        if (color == "red") col = 4;
        if (color == "green") col = 2;
        if (color == "yellow") col = 6;
        if (color == "blue") col = 1;
        if (color == "magenta") col = 5;
        if (color == "cyan") col = 3;
        if (color == "gray") col = 8;
        if (color == "white") col = 7;
        if (background.size()) {
            if (background == "black") bg = 0;
            if (background == "red") bg = 64;
            if (background == "green") bg = 32;
            if (background == "yellow") bg = 96;
            if (background == "blue") bg = 16;
            if (background == "magenta") bg = 80;
            if (background == "cyan") bg = 48;
            if (background == "gray") bg = 128;
            if (background == "white") bg = 112;
        }
        if (color.length() > 3)
        if (color.substr(0, 3) == "rgb") {
            std::string text = color.substr(4, color.length());
            std::vector<std::string> nums;
            std::string num = "";
            for (char &sym : text) {
                if (sym == ',') {
                    nums.push_back(num);
                    num = "";
                    continue;
                }
                if (sym == ')') {
                    nums.push_back(num);
                    break;
                }
                if (sym != ' ') {
                    num += sym;
                }
            }
            // If there is a background
            if (nums.size() >= 6) {
                if (std::stoi(nums[3]) > std::stoi(nums[4]) + std::stoi(nums[5])) bg = 64;
                if (std::stoi(nums[4]) > std::stoi(nums[3]) + std::stoi(nums[5])) bg = 32;
                if (std::stoi(nums[5]) > std::stoi(nums[4]) + std::stoi(nums[3])) bg = 16;
                if (std::stoi(nums[5]) > 200 && std::stoi(nums[4]) > 200 && std::stoi(nums[3]) > 200) bg = 112;
                if (std::stoi(nums[5]) < 50 && std::stoi(nums[4]) < 50 && std::stoi(nums[3]) < 50) bg = 0;
            }
            // If there is a color
            if (nums.size() >= 3) {
                if (std::stoi(nums[0]) > std::stoi(nums[1]) + std::stoi(nums[2])) col = 4;
                if (std::stoi(nums[1]) > std::stoi(nums[0]) + std::stoi(nums[2])) col = 2;
                if (std::stoi(nums[2]) > std::stoi(nums[1]) + std::stoi(nums[0])) col = 1;
                if (std::stoi(nums[5]) > 200 && std::stoi(nums[4]) > 200 && std::stoi(nums[3]) > 200) col = 7;
                if (std::stoi(nums[5]) < 50 && std::stoi(nums[4]) < 50 && std::stoi(nums[3]) < 50) col = 0;
            }
        }
        if (color == "natural") SetConsoleTextAttribute(hConsole, 7);
        else SetConsoleTextAttribute(hConsole, bg + col);
        std::cout << line;
        SetConsoleTextAttribute(hConsole, 7);
        return "";
        #endif
    }

    // Style the font
    if (color.length() > 2)
    if (color[1] == ':') {
        if (color[0] == 'b') style = "1;";
        if (color[0] == 'd') style = "2;";
        if (color[0] == 'i') style = "3;";
        if (color[0] == 'u') style = "4;";
        color = color.substr(2);
    }

    // Split color to
    // foreground and background
    int found = color.find(':');
    if (found != std::string::npos) {
        background = color.substr(found + 1);
        color = color.substr(0, found);
    }

    // Save style to a separate
    // varialbe which is a bash
    // wird behavior workaround
    tmp = style;

    // Bash bug regarding weird
    // behavior with background color
    if (background.length()) {
        style = "";
    }

    // Foreground color + optional style
    if (color == "black") col = "\033["+style+"30m";
    if (color == "red") col = "\033["+style+"31m";
    if (color == "green") col = "\033["+style+"32m";
    if (color == "yellow") col = "\033["+style+"33m";
    if (color == "blue") col = "\033["+style+"34m";
    if (color == "magenta") col = "\033["+style+"35m";
    if (color == "cyan") col = "\033["+style+"36m";
    if (color == "gray") col = "\033["+style+"37m";
    if (color == "white") col = "\033["+style+"97m";

    // Get style from possibly
    // hidden foreground style
    style = tmp;

    // Check if some background is there too
    if (background.size()) {
        // Background color + optional style
        if (background == "black") bg = "\033["+style+"40m";
        if (background == "red") bg = "\033["+style+"41m";
        if (background == "green") bg = "\033["+style+"42m";
        if (background == "yellow") bg = "\033["+style+"43m";
        if (background == "blue") bg = "\033["+style+"44m";
        if (background == "magenta") bg = "\033["+style+"45m";
        if (background == "cyan") bg = "\033["+style+"46m";
        if (background == "gray") bg = "\033["+style+"47m";
        if (background == "white") bg = "\033["+style+"107m";
    }

    // RGB Function functionality
    if (color.length() > 3)
    if (color.substr(0, 3) == "rgb") {
        std::string text = color.substr(4, color.length());
        std::vector<std::string> nums;
        std::string num = "";
        for (char &sym : text) {
            if (sym == ',') {
                nums.push_back(num);
                num = "";
                continue;
            }
            if (sym == ')') {
                nums.push_back(num);
                break;
            }
            if (sym != ' ') {
                num += sym;
            }
        }
        col = "";
        // If there is a background
        if (nums.size() >= 6)
            col += "\x1b[48;2;"+nums[3]+";"+nums[4]+";"+nums[5]+"m";
        // If there is a color
        if (nums.size() >= 3)
            col += "\x1b[38;2;"+nums[0]+";"+nums[1]+";"+nums[2]+"m";
        // Returned colored string
        return std::string("\033["+std::string(1, style[0])+"m") + col + line + "\x1b[0m\033[0m";
    }

    return bg + col + line + "\033[0m";
}
