// Process class responsible for
// information about current process
class Process {
public:

    static Mix<Text> args;
    static Text os;
    static void init(int argc, char** argv);

};

#ifdef _WIN32
    #define MAIN(name) \
    int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) { \
        { \
            Alloy::Process::init(__argc, __argv); \
        } \
        try { \
            name(); \
        } \
        catch(Failure fail) { \
            Alloy::Text fun = "\n"; \
            if (fail.function.len()) { \
                fun = Alloy::Text(" in function: @()\n", {fail.function}); \
            } \
            int msgboxID = MessageBox( \
                NULL, \
                Text("[code: @]@@", {fail.code, fun, fail.reason}).cstr(), \
                "Runtime Error", \
                MB_ICONERROR | MB_OK \
            ); \
        } \
        return 0; \
    }
#else
    #define MAIN(name) \
        int main(int argc, char** argv) { \
            Alloy::Process::init(argc, argv); \
            try { \
                name(); \
            } \
            catch(Failure fail) { \
                fail.what(); \
            } \
            return 0; \
        }
#endif
