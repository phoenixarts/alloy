RegExp::RegExp(Text reg) {
    this->regexp = std::regex((std::string)reg, std::regex_constants::ECMAScript);
}

// Check if regexp applies to the whole text
Switch RegExp::match(Text line) {
    return std::regex_match((std::string)line, this->regexp);
}

// Check if regexp applies to part of the text
Switch RegExp::search(Text line) {
    return std::regex_search((std::string)line, this->regexp);
}

// Get matched groups with different combinations
Mix<Mix<Text>> RegExp::groups(Text line) {
    std::smatch matches;
    Mix<Mix<Text>> result;
    std::string ln = (std::string)line;
    while (std::regex_search (ln, matches, this->regexp)) {
        Mix<Text> cur;
        for (auto x : matches) {
            cur.join(Text(x));
        }
        result.join(cur);
        line = Text(matches.suffix().str());
    }
    return result;
}
