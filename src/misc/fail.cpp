Failure::Failure() {}

// Take failure code and reason
Failure::Failure(Number code, Text reason) {
    this->code = code;
    this->reason = reason;
}

// Take failure code and reason (Function name as experimental)
Failure::Failure(Number code, Text reason, Text function) {
    this->code = code;
    this->reason = reason;
    this->function = function;
}

// Behavior of actual failure
const char* Failure::what() const throw () {
    // Runtime error BAND
    std::cout << "\n\033[1m\x1b[48;2;255;0;0m\x1b[38;2;0;0;0m";
    std::cout << " RUNTIME ERROR ";
    std::cout << "\x1b[0m";
    // Show error code
    std::cout << "\033[1;31m";
    std::cout << " [code: " << this->code.value << "]";
    std::cout << "\033[0;31m";
    // Show function name if provided
    if (this->function.value.length()) {
        std::cout << " In function: ";
        std::cout << "\x1b[38;2;100;150;255m";
        std::cout << this->function.value;
        std::cout << "\x1b[38;2;100;100;100m";
        std::cout << "()\n";
        std::cout << "\x1b[0m";
        std::cout << "\033[0;31m";
    }
    else {
        std::cout << "\n";
    }
    // Show failure reason
    std::cout << this->reason.value << "\n";
    std::cout << "\033[0m";
    // Exit this binary
    return "";
}