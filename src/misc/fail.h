// Alloy main fail class
class Failure : public std::exception {
public:
    Number code = 0;
    Text reason = "";
    Text function = "";

    Failure();
    Failure(Number code, Text reason);
    Failure(Number code, Text reason, Text function);
    virtual const char* what() const throw ();
};
