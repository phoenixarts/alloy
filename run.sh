# Library name
name=alloy
# Library header path
header=src/alloy.h
# Library source path
source=src/alloy.cpp
# Library build directory path
dist=./build

optimize=""

if [[ "$1" == "--prod" ]]; then
    echo "    --- PRODUCTION ---"
    optimize="-Ofast"
fi

# Linux case
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    echo "--- Compiling for linux ---"
    clang++ -c ${source} -fPIC -o ${dist}/tmp.o -ferror-limit=2 -Wfatal-errors -std=c++2a
    llvm-ar rcs ${dist}/lib${name}.a ${dist}/tmp.o
    python3 headerize.py ${header} ${dist}/${name}.hpp
    rm ${dist}/tmp.o
    clang++ test/test.cpp ${dist}/liballoy.a -I ${dist} -o app.out -ferror-limit=2 -Wfatal-errors -std=c++2a -ldl $optimize
    ./app.out
# Windows case
elif [[ "$OSTYPE" == "msys" ]]; then
    echo "--- Compiling for windows ---"
    clang++ -c ${source} -o ${dist}/tmp.o -ferror-limit=2 -Wfatal-errors -std=c++2a -Wno-deprecated-declarations
    llvm-ar rcs ${dist}/lib${name}.lib ${dist}/tmp.o
    python headerize.py ${header} ${dist}/${name}.hpp
    rm ${dist}/tmp.o
    clang++ test/test.cpp ${dist}/liballoy.lib -I ${dist} -o app1.exe -ferror-limit=2 -Wfatal-errors -std=c++2a -lShell32 -lUser32 -Wno-deprecated-declarations $optimize
    ./app1.exe
fi
